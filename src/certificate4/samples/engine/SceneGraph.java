package certificate4.samples.engine;

import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;

/**
 *
 * rootNode is the "center of the universe". Everything is relative to its position. This pattern/rule is recursive
 * through the scene graph tree data structure.
 *
 * @author Roland Fuller
 */
public class SceneGraph extends SimpleApplication {

    public static final Vector3f ONE_TO_THE_RIGHT = new Vector3f(1, 0, 0);

    public static void main(String[] args) {
        SceneGraph app = new SceneGraph();
        app.start();
    }

    @Override
    public void simpleInitApp() {
        makeBoxToTheRightOfCenter();
        makeBoxTwoToTheRight();
        makeBoxContainerRotated();
        makeBoxWhichDoesntExist();
        makeBoxWithRotatedGeometryInside();
    }

    /**
     * Creates a node which is attached to the root (no offset) Attaches a box within itself that is locally offset by
     * one on the X
     */
    public void makeBoxToTheRightOfCenter() {
        Node a = new Node();
        a.attachChild(makeBox(ONE_TO_THE_RIGHT, ColorRGBA.Green));
        rootNode.attachChild(a);
    }

    /**
     * Make a node which is offset to the right of center, then attach to it a box which is offset to the right of its
     * container's center.
     */
    public void makeBoxTwoToTheRight() {
        Node b = new Node();
        b.setLocalTranslation(ONE_TO_THE_RIGHT); // move relative to the rootnode
        b.attachChild(makeBox(ONE_TO_THE_RIGHT, ColorRGBA.Red));
        rootNode.attachChild(b);
    }

    /**
     * Attach our container to the center of root and add a model offset to the right of it and then rotate the whole
     * container, flipping the positive X offset to a negative one
     */
    public void makeBoxContainerRotated() {
        Node c = new Node();
        c.attachChild(makeBox(ONE_TO_THE_RIGHT, ColorRGBA.Blue));
        c.rotate(new Quaternion(new float[]{0, 0, 180 * FastMath.DEG_TO_RAD}));
        rootNode.attachChild(c);
    }

    public void makeBoxWhichDoesntExist() {
        Node d = new Node();
        d.attachChild(makeBox(ONE_TO_THE_RIGHT, ColorRGBA.Yellow));
        //rootNode.attachChild(d); // d is not attached to the rootNode, therefore it isn't rendered
    }

    /**
     * Create container at the origin and give it a box which also isn't offset. Rotate the container, this time
     * rotating the model on the spot as it isn't offset from its container.
     */
    public void makeBoxWithRotatedGeometryInside() {
        Node e = new Node();
        e.attachChild(makeBox(Vector3f.ZERO, ColorRGBA.Brown));
        e.rotate(new Quaternion(new float[]{0, 0, 56 * FastMath.DEG_TO_RAD}));
        rootNode.attachChild(e);
    }

    /**
     * Make a simple box model and return it to caller.
     *
     * @param location where to place it, locally
     * @param colour the colour of the material
     * @return the completed model.
     */
    public Spatial makeBox(Vector3f location, ColorRGBA colour) {
        Box b = new Box(0.5f, 0.5f, 0.5f);
        Geometry geom = new Geometry("Box", b);
        geom.setLocalTranslation(location);

        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", colour);
        geom.setMaterial(mat);

        return geom;
    }
}
