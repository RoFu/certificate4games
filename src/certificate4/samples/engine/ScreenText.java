package certificate4.samples.engine;

import com.jme3.app.SimpleApplication;
import com.jme3.font.BitmapText;

/**
 * @author Roland Fuller
 */
public class ScreenText extends SimpleApplication {

    private static final int NUM_LINES = 25; // the number of text lines we want 
    private BitmapText[] textLines; // An array of BitmapText objects. Each one holds a text string to display

    /**
     * Entry point of the program
     *
     * @param args
     */
    public static void main(String[] args) {
        ScreenText app = new ScreenText();// creates an object of this class's type and...
        app.start(); // passes over control to the inherited engine method, which fires off the game loop
    }

    @Override
    public void simpleInitApp() {
        textLines = new BitmapText[NUM_LINES]; // initialise the array to hold this many objects
        
        // a number of times we repeat...
        for (int i = 0; i < NUM_LINES; i++) {
            textLines[i] = makeTextLine(i); // creating a text object and adding it to the array
        }

        // Now we use our utility method to set the text of a given line
        setLineText(0, "Hello there");
        setLineText(1, "Welcome to my simple game");
        setLineText(2, "It is a number guessing game.");
        setLineText(25, "Why does this line result in failure?!");
    }

    /**
     * Create a bitmap text object at a given location
     *
     * @param num the line number so we can place it on the screen
     * @return the created bitmap text object
     */
    public BitmapText makeTextLine(int num) {
        BitmapText line = new BitmapText(guiFont, false); // create the bitmaptext object with a default font in left to right reading order
        line.setSize(guiFont.getCharSet().getRenderedSize()); // set the size of the text for this bitmaptext to the native size of the font
        line.setLocalTranslation(0, settings.getHeight() - (num * line.getSize()), 0); // position the text on the screen according to the resolution and line number
        guiNode.attachChild(line); // we need to attach the bitmaptext to the guiNode so it will be rendered
        return line; // return to caller with the finished bitmaptext
    }

    /**
     * A utility method to safely set the text of a given bitmaptext by line number
     *
     * @param lineNumber the line number to be setting
     * @param text the text to place at this position
     */
    private void setLineText(int lineNumber, String text) {
        // the try block attempts to do something which may be unsafe
        try {
            textLines[lineNumber].setText(text); // note the given index may not exist
        } catch (ArrayIndexOutOfBoundsException aiobe) { // what to do if this particular exception occurred
            System.out.println("Can't set text line " + lineNumber + " because there are only " + textLines.length + " text lines!");
        }
    }
}
