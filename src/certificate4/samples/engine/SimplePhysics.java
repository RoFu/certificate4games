package certificate4.samples.engine;

import com.jme3.app.SimpleApplication; // we base our game class off this
import com.jme3.bullet.BulletAppState; // this represents the physics engine
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.collision.shapes.SphereCollisionShape;
import com.jme3.bullet.control.RigidBodyControl; // physics controller controls movement and collision of an object
import com.jme3.math.Vector3f; // represents a location in 3d space
import com.jme3.scene.Node; // container for a game object in the world

/**
 * A simple demonstration of the concepts of setting up the physics engine and adding some collidable shapes to the
 * world Note this example does not feature visible models. We can only see the wireframes of the collision shapes
 * because of the physics debugging option being switched on in the init method.
 *
 * @author Roland Fuller
 */
public class SimplePhysics extends SimpleApplication {

    private static final float SPAWN_TIME = 1.0f; // what we call a "constant"
    private float spawnTimer; // a simple timer to add new objects
    private BulletAppState bulletAppState; // reference to the physics engine object

    /**
     * Entry point of the program
     *
     * @param args
     */
    public static void main(String[] args) {
        SimplePhysics app = new SimplePhysics();// creates an object of this class's type and...
        app.start(); // passes over control to the inherited engine method, which fires off the game loop
    }

    /**
     * The first method which is called in your game. Used to setup the state of the world e.g create all your entities
     * in the correct position with the right configuration ready to play
     */
    @Override
    public void simpleInitApp() {
        flyCam.setMoveSpeed(100); // set default camera movement speed faster
        bulletAppState = new BulletAppState(); // create physics engine
        bulletAppState.setDebugEnabled(true); // show wireframe of collision shapes
        stateManager.attach(bulletAppState); // make sure physics engine is plugged into our game
        makeGround(new Vector3f(0, -10, 0)); // create an entity
    }

    /**
     * A collection of steps required to create a entity which has a model, material, name, location, and physics
     * controller.
     *
     * @param location
     */
    private void makeSphere(Vector3f location) {
        Node node = new Node("Sphere");
        node.setLocalTranslation(location);
        node.addControl(new RigidBodyControl(new SphereCollisionShape(1), 1)); // tell the physics controller which entity to move
        bulletAppState.getPhysicsSpace().add(node); // allow the physics controller to talk to all other physical objects
        rootNode.attachChild(node); // attaching the entity container to the rootNode adds it to the world
    }

    /**
     * This method looks very similar to the one above. There is a lot of duplication going on here which would ideally
     * be refactored, so the similar code can be shared between the two methods. only the major differences are
     * highlighted.
     */
    private void makeGround(Vector3f location) {
        Node node = new Node("Ground");
        node.setLocalTranslation(location);
        node.addControl(new RigidBodyControl(new BoxCollisionShape(new Vector3f(10, 1, 10)), 0));
        bulletAppState.getPhysicsSpace().add(node);
        rootNode.attachChild(node);
    }

    @Override
    public void simpleUpdate(float tpf) {
        super.simpleUpdate(tpf);
        spawnTimer += tpf;
        // when the time limit has been exceeded, spawn a sphere
        if (spawnTimer >= SPAWN_TIME) {
            spawnTimer = 0; // reset timer to zero to time again
            makeSphere(Vector3f.ZERO); // create a new sphere
        }
    }

}
