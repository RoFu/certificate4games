package certificate4.samples.engine;

import com.jme3.app.SimpleApplication;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.light.AmbientLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.shape.Sphere;
import com.jme3.scene.shape.Torus;
import com.jme3.shader.VarType;
import com.jme3.texture.Texture;

/**
 * Make a simple moving box.
 *
 * @author Roland Fuller
 */
public class SolarSystem extends SimpleApplication {

    private static class SpinControl extends AbstractControl {

        private float speed;

        public SpinControl(float speed) {
            this.speed = speed;
        }

        @Override
        protected void controlUpdate(float tpf) {
            spatial.rotate(0, 0, tpf * speed);
        }

        @Override
        protected void controlRender(RenderManager rm, ViewPort vp) {
        }
    }

    public static void main(String[] args) {
        Controllers app = new Controllers();
        app.start();
    }

    @Override
    public void simpleInitApp() {
        flyCam.setMoveSpeed(100);
        cam.setLocation(new Vector3f(0, 0, 400));
        rootNode.addLight(new AmbientLight(ColorRGBA.White));

        final Node sun = makeSphere(new ColorRGBA(1, 1, 0, 0), new Vector3f(0, 0, 0), 25);
        sun.attachChild(makeParticles());
        rootNode.attachChild(sun);

        makeBody("Mercury", rootNode, new ColorRGBA(0.2f, 0.2f, 0.2f, 0), 30, 1.5f, 1);
        makeBody("Venus", rootNode, new ColorRGBA(0.75f, 0.75f, 0, 0), 40, 2, 0.9f);

        Node earth = (Node) makeBody("Earth", rootNode, new ColorRGBA(0, 0.5f, 0.5f, 0), 50, 2.25f, 0.75f).getChild(0);
        makeBody("Moon", earth, new ColorRGBA(0.75f, 0.75f, 0.75f, 0), 4, 0.8f, 2);

        Node mars = (Node) makeBody("Mars", rootNode, new ColorRGBA(0.3f, 0, 0, 0), 70, 1.9f, 0.9f).getChild(0);
        makeBody("Phobos", mars, new ColorRGBA(0.75f, 0.75f, 0.75f, 0), 2, 0.6f, 1);
        makeBody("Deimos", mars, new ColorRGBA(0.75f, 0.75f, 0.75f, 0), 4, 0.4f, 2);

        makeBody("Jupiter", rootNode, new ColorRGBA(1f, 0.6f, 0, 0), 90, 8, 0.5f);

        Node saturn = (Node) makeBody("Saturn", rootNode, new ColorRGBA(0.8f, 0.8f, 0, 0), 120, 4, 0.3f).getChild(0);
        saturn.attachChild(makeRing(ColorRGBA.Yellow, Vector3f.ZERO, 8));

        makeBody("Uranus", rootNode, new ColorRGBA(0, 0.1f, 0.9f, 0), 140, 3, 0.2f);
        makeBody("Neptune", rootNode, new ColorRGBA(0, 0.25f, 1, 0), 170, 5, 0.1f);

    }

    public Node makeBody(String name, Node parent, ColorRGBA colour, float distance, float radius, float speed) {
        Node orbit = new Node(name);
        orbit.attachChild(makeSphere(colour, new Vector3f(distance, 0, 0), radius));
        orbit.addControl(new SpinControl(speed));
        parent.attachChild(orbit);
        return orbit;
    }

    public Node makeSphere(ColorRGBA colour, Vector3f location, float radius) {
        Node container = new Node();
        container.addControl(new SpinControl(0.5f));

        Sphere b = new Sphere(32, 32, radius);
        Geometry geom = new Geometry("Box", b);
        container.setLocalTranslation(location);
        container.attachChild(geom);

        Material mat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");

        final Texture diffuseTexture = assetManager.loadTexture("Textures/Terrain/Rock/Rock.PNG");
        mat.setTexture("DiffuseMap", diffuseTexture);

        final Texture normalTexture = assetManager.loadTexture("Textures/Terrain/Rock/Rock_normal.png");
        mat.setTexture("NormalMap", normalTexture);
        mat.setParam("UseMaterialColors", VarType.Boolean, true);
        mat.setColor("Ambient", colour);
        geom.setMaterial(mat);

        return container;
    }

    public Node makeRing(ColorRGBA colour, Vector3f location, float radius) {
        Node container = new Node();

        Torus t = new Torus(32, 32, radius / 8, radius);
        Geometry geom = new Geometry("Ring", t);
        container.setLocalTranslation(location);
        container.attachChild(geom);

        Material mat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");

        final Texture diffuseTexture = assetManager.loadTexture("Textures/Terrain/Rock/Rock.PNG");
        mat.setTexture("DiffuseMap", diffuseTexture);

        final Texture normalTexture = assetManager.loadTexture("Textures/Terrain/Rock/Rock_normal.png");
        mat.setTexture("NormalMap", normalTexture);
        mat.setParam("UseMaterialColors", VarType.Boolean, true);
        mat.setColor("Ambient", colour);
        geom.setMaterial(mat);

        return container;
    }

    private Spatial makeParticles() {
        Node container = new Node();
        ParticleEmitter fire
                = new ParticleEmitter("Emitter", ParticleMesh.Type.Triangle, 50);
        Material mat_red = new Material(assetManager,
                "Common/MatDefs/Misc/Particle.j3md");
        mat_red.setTexture("Texture", assetManager.loadTexture(
                "Effects/Explosion/flame.png"));
        fire.setMaterial(mat_red);
        fire.setImagesX(2);
        fire.setImagesY(2); // 2x2 texture animation
        fire.setEndColor(new ColorRGBA(1f, 0f, 0f, 1f));   // red
        fire.setStartColor(new ColorRGBA(1f, 1f, 0f, 0.5f)); // yellow
        fire.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 0, 15));
        fire.setStartSize(55f);
        fire.setEndSize(1f);
        fire.setGravity(0, 0, 0);
        fire.setLowLife(1f);
        fire.setHighLife(3f);
        fire.getParticleInfluencer().setVelocityVariation(0.3f);

        container.attachChild(fire);
        return container;
    }

}
