package certificate4.samples.engine;

import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.collision.shapes.SphereCollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import java.util.Random;

/**
 * A more complex example of physics where we create a hollow rotating box full of balls.
 *
 * @author Roland Fuller
 */
public class BallsInBoxPhysics extends SimpleApplication {

    private BulletAppState bulletAppState; // reference to the physics engine object

    /**
     * Entry point of the program
     *
     * @param args
     */
    public static void main(String[] args) {
        BallsInBoxPhysics app = new BallsInBoxPhysics(); // creates an object of this class's type and...
        app.start(); // passes over control to the inherited engine method, which fires off the game loop
    }

    /**
     * The first method which is called in your game. Used to setup the state of the world e.g create all your entitys
     * in the correct position with the right configuration ready to play
     */
    @Override
    public void simpleInitApp() {
        cam.setLocation(new Vector3f(0, 0, 500));
        flyCam.setMoveSpeed(100); // set default camera movement speed faster
        bulletAppState = new BulletAppState(); // create physics engine
        bulletAppState.setDebugEnabled(true); // show wireframe of collision shapes
        stateManager.attach(bulletAppState); // make sure physics engine is plugged into our game
        makeBox();
        makeBunchOfBalls();
    }

    /**
     * Create many spheres
     */
    private void makeBunchOfBalls() {
        for (int i = 0; i < 100; i++) {
            makeSphere(getRandomLocation(50, 50, 50));
        }
    }

    /**
     * Create a random location within a boxed in area
     *
     * @param x size of the area's axis
     * @param y size of the area's axis
     * @param z size of the area's axis
     * @return a Vector3f of this random location
     */
    private Vector3f getRandomLocation(int x, int y, int z) {
        return new Vector3f(getPosNegInt(x), getPosNegInt(y), getPosNegInt(z));
    }

    /**
     *
     * @param range
     * @return a negative or positive integer from zero to the range
     */
    public int getPosNegInt(int range) {
        return new Random().nextBoolean() ? new Random().nextInt(range) : -new Random().nextInt(range);
    }

    /**
     * A collection of steps required to create a entity which has a model, material, name, location, and physics
     * controller.
     *
     * @param location
     */
    private void makeSphere(Vector3f location) {
        Node node = new Node(); // create a container for the physics object in the world
        node.setLocalTranslation(location); // move our entity to the specified location
        node.addControl(new RigidBodyControl(new SphereCollisionShape(10), 100)); // Create a physics controller and attach directly to the container. Also give it a shape and mass.
        bulletAppState.getPhysicsSpace().add(node); // allow the physics controller to talk to all other physical objects
        rootNode.attachChild(node); // attaching the entity container to the rootNode adds it to the world
    }

    /**
     * Creates a box of 6 walls with their own physics controllers
     */
    private void makeBox() {
        Node node = new Node("Box"); // make a container to put all the walls of our box into to group it as one
        node.addControl(new RotatorControl()); // tell this container to rotate according to our control class
        node.attachChild(makeWall(new Vector3f(-100, 0, 0), Vector3f.UNIT_Z)); // generate each of the walls
        node.attachChild(makeWall(new Vector3f(100, 0, 0), Vector3f.UNIT_Z));
        node.attachChild(makeWall(new Vector3f(0, -100, 0), null));
        node.attachChild(makeWall(new Vector3f(0, 100, 0), null));
        node.attachChild(makeWall(new Vector3f(0, 0, -100), Vector3f.UNIT_X));
        node.attachChild(makeWall(new Vector3f(0, 0, 100), Vector3f.UNIT_X));
        rootNode.attachChild(node); // add the box to the world
    }

    /**
     * Create the wall of a box
     *
     * @param location the location relative to the center of the box
     * @param axis to rotate 90 degrees around. If null, no rotation performed.
     * @return
     */
    private Spatial makeWall(Vector3f location, Vector3f axis) {
        Node node = new Node();
        if (axis != null) {
            Quaternion quaternion = new Quaternion();
            quaternion.fromAngleAxis(FastMath.DEG_TO_RAD * 90, axis);
            node.rotate(quaternion);
        }
        node.setLocalTranslation(location); // has to happen after rotation - translate/rotate operations are not commutative
        RigidBodyControl rigidBodyControl = new RigidBodyControl(new BoxCollisionShape(new Vector3f(100, 1, 100)), 0); // mass zero means immovable by collisions/gravity
        node.addControl(rigidBodyControl);
        rigidBodyControl.setKinematic(true); // kinematic is a requirement as we want the node's controller to rotate the physical objects as well. Has to be set after adding to node.
        bulletAppState.getPhysicsSpace().add(node);
        return node;
    }

    /**
     * A very simple controller class which will just spin whatever it's added to
     */
    private static class RotatorControl extends AbstractControl {

        @Override
        protected void controlUpdate(float tpf) {
            spatial.rotate(tpf, tpf * 0.25f, 0);
        }

        @Override
        protected void controlRender(RenderManager rm, ViewPort vp) {
        }

    }

}
