package certificate4.samples.engine;

import com.jme3.app.SimpleApplication;// we base our game class off this
import com.jme3.material.Material;// the surface properties of a mesh including textures etc
import com.jme3.math.ColorRGBA; // defines a colour with transparency
import com.jme3.math.Vector3f; // represents a location in 3d space
import com.jme3.scene.Geometry; // represents a model at a location
import com.jme3.scene.shape.Box; // class which generates a cube mesh

/**
 * Here we create a chess board of cubes using a nested for loop for the positions calling upon a "factory" method
 * makeBox to create individual models.
 *
 * @author Roland Fuller
 */
public class PrimitiveGeometry extends SimpleApplication {

    public static void main(String[] args) {
        PrimitiveGeometry app = new PrimitiveGeometry();
        app.start();
    }

    @Override
    public void simpleInitApp() {
        cam.setLocation(new Vector3f(4.5f, 4.5f, 15));

        ColorRGBA colour;
        for (int x = 0; x < 10; x++) {
            for (int y = 0; y < 10; y++) {
                if ((x + y) % 2 == 0) {
                    colour = ColorRGBA.Blue;
                } else {
                    colour = ColorRGBA.Red;
                }
                makeBox(new Vector3f(x, y, 0), colour);
            }
        }
    }

    public void makeBox(Vector3f location, ColorRGBA colour) {
        Box b = new Box(0.5f, 0.5f, 0.5f);
        Geometry geom = new Geometry("Box", b);
        geom.setLocalTranslation(location);

        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", colour);
        geom.setMaterial(mat);

        rootNode.attachChild(geom);
    }

}
