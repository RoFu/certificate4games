package certificate4.samples.engine;

import com.jme3.app.SimpleApplication;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import java.util.HashMap;

/**
 * Adding implements statements in the class definition means this class will
 * function as a listener for both analog and digital input events It forces the
 * class to implement the onAction and onAnalog methods seen below, which means
 * we can map "this" class as an input listener of the inputManager
 *
 * @author Roland Fuller
 */
public class HandlingInput extends SimpleApplication implements ActionListener, AnalogListener { // make sure the ActionListener imported is from the jme3 package - not AWT!

    private HashMap<String, Float> keyTimers;

    /**
     * Entry point of the program
     *
     * @param args
     */
    public static void main(String[] args) {
        HandlingInput app = new HandlingInput();// creates an object of this class's type and...
        app.start(); // passes over control to the inherited engine method, which fires off the game loop
    }

    @Override
    public void simpleInitApp() {
        keyTimers = new HashMap<>();
        inputManager.addListener(this, "up", "down", "left", "right", "mouse up", "mouse down", "mouse click"); // "this" class is the recipient of these input events. Note, the strings are arbitrary.
        // following are setting up input mappings in the input Manager. We're telling it to register the names with specific device triggers
        inputManager.addMapping("up", new KeyTrigger(KeyInput.KEY_UP)); // pair the "up" action with the keyboard up arrow
        inputManager.addMapping("down", new KeyTrigger(KeyInput.KEY_DOWN));// pair the "down" action with the keyboard down arrow
        inputManager.addMapping("left", new KeyTrigger(KeyInput.KEY_LEFT));// pair the "left" action with the keyboard left arrow
        inputManager.addMapping("right", new KeyTrigger(KeyInput.KEY_RIGHT));// pair the "right" action with the keyboard right arrow
        inputManager.addMapping("mouse up", new MouseAxisTrigger(MouseInput.AXIS_Y, false)); // pair the "mouse up" action to the mouse's Y axis negative
        inputManager.addMapping("mouse down", new MouseAxisTrigger(MouseInput.AXIS_Y, true)); // pair the "mouse down" action to the mouse's Y axis positive
        inputManager.addMapping("mouse click", new MouseButtonTrigger(MouseInput.BUTTON_LEFT)); // pair the "mouse click" action to the mouse's Y left button
    }

    /**
     * Called when an input to which this listener is registered to is invoked.
     *
     * @param name The name of the mapping that was invoked (the names we made
     * up for our input mappings above, e.g "mouse click")
     * @param isPressed True if the action is "pressed", false otherwise
     * @param tpf The time per frame value. (how long it's been since the last
     * time we were called)
     */
    @Override
    public void onAction(String name, boolean isPressed, float tpf) {
        System.out.println(isPressed ? "Pressed " + name : "Released " + name);
    }

    /**
     * The results of KeyTrigger and MouseButtonTrigger events will have tpf ==
     * value.
     *
     * @param name The name of the mapping that was invoked (the names we made
     * up for our input mappings above, e.g "mouse click")
     * @param value Value of the axis, from 0 to 1. (only makes sense for
     * joysticks/triggers/mouse/etc not keyboard)
     * @param tpf The time per frame value. (accumulate it to figure out how
     * long the action is held)
     */
    @Override
    public void onAnalog(String name, float value, float tpf) {
        System.out.println("Holding " + name + " this much " + value + " for time " + tpf);
        keyTimers.put(name, keyTimers.get(name) + tpf);
    }

}
