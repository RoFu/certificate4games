package certificate4.samples.engine;

import com.jme3.app.SimpleApplication;

/**
 * A simple method of timing in our game by accumulating the time per frame value in the update method.
 *
 * @author Roland Fuller
 */
public class Timer extends SimpleApplication {

    private float timer; // simple counter variable used to accumulate time

    /**
     * Entry point of the program
     *
     * @param args
     */
    public static void main(String[] args) {
        Timer app = new Timer();// creates an object of this class's type and...
        app.start(); // passes over control to the inherited engine method, which fires off the game loop
    }

    @Override
    public void simpleInitApp() {

    }

    @Override
    public void simpleUpdate(float tpf) {
        timer += tpf; // add the time since last update to our running counter
        System.out.println("Elapsed game time: " + timer); // Display message on the console
    }

}
