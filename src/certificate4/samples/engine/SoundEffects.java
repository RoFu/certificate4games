package certificate4.samples.engine;

import com.jme3.app.SimpleApplication;
import com.jme3.audio.AudioData;
import com.jme3.audio.AudioNode;

/**
 * @author Roland Fuller
 */
public class SoundEffects extends SimpleApplication {

    private static final float SOUND_TIME = 2; // timing limit for sound play
    private float soundTimer; // timer variable to control timing of sound play
    private AudioNode sound; // variable to load sound effect into
    private AudioNode music; // variable to load music track into

    /**
     * Entry point of the program
     *
     * @param args
     */
    public static void main(String[] args) {
        SoundEffects app = new SoundEffects();// creates an object of this class's type and...
        app.start(); // passes over control to the inherited engine method, which fires off the game loop
    }

    @Override
    public void simpleInitApp() {
        sound = loadSoundEffect("Sound/Effects/Beep.ogg");
        loadMusicTrack("Sound/Environment/Nature.ogg");
    }

    /**
     * Utility method to load a sound effect. Doesn't play it. Note that the sound variable shadows the class member
     * variable also called sound.
     *
     * @param filePath the location of the sound file
     * @return the loaded sound effect object
     */
    private AudioNode loadSoundEffect(String filePath) {
        AudioNode sound = new AudioNode(assetManager, filePath, AudioData.DataType.Buffer); // cache the whole data in memory
        sound.setPositional(false); // positional sounds require more code, so switch it off.
        sound.setVolume(0.05f); // this could be a parameter, better yet normalise all sounds in audacity
        rootNode.attachChild(sound); // must attach sound node to the world or it won't play
        return sound;
    }

    /**
     * Utility method to load a music/background track. Plays in infinite loop.
     *
     * @param filePath the sound file to load
     */
    private void loadMusicTrack(String filePath) {
        music = new AudioNode(assetManager, filePath, AudioData.DataType.Stream); // more memory efficient to stream from disk
        music.setLooping(true); // positional sounds require more code, so switch it off.
        music.setPositional(false); // positional sounds require more code, so switch it off.
        music.setVolume(20.0f); // this could be a parameter, better yet normalise all sounds in audacity
        rootNode.attachChild(music); // must attach sound node to the world or it won't play
        music.play(); // start playing the music
    }

    @Override
    public void simpleUpdate(float tpf) {
        super.simpleUpdate(tpf);
        soundTimer += tpf; // accumulate time
        if (soundTimer >= SOUND_TIME) { // when timer expires
            soundTimer = 0; // reset timer
            sound.playInstance(); // play the sound.
        }
    }

}
