package certificate4.samples.engine;

import com.jme3.app.SimpleApplication;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.light.PointLight;
import com.jme3.light.SpotLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.post.FilterPostProcessor;
import com.jme3.post.ssao.SSAOFilter;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.LightControl;
import com.jme3.shadow.EdgeFilteringMode;
import com.jme3.shadow.PointLightShadowRenderer;
import com.jme3.util.SkyFactory;
import com.jme3.util.SkyFactory.EnvMapType;
import com.jme3.util.TangentBinormalGenerator;
import jme3tools.optimize.GeometryBatchFactory;

/**
 * How to import Blender models: 1) Copy the *.blend file into the Project
 * Assets/models directory along with any texture files 2) Right click on the
 * *.blend file and choose "convert to .j3o" 3) You can delete the original
 * *.blend file, or continue to work on it in this folder and just update the
 * j3o when changes are made. 4) Lighting must be available in order to view
 * materials!
 *
 * @author Roland Fuller
 */
public class BlenderModels extends SimpleApplication {

    // Configuration for the spotlight (can have many but sample only uses one)
    public static final float SPOTLIGHT_OUTER_ANGLE = 90f;
    public static final float SPOTLIGHT_INNER_ANGLE = 0f;
    public static final ColorRGBA SPOTLIGHT_COLOUR = ColorRGBA.Blue;
    public static final float SPOTLIGHT_RANGE = 100.0f;
    public static final Vector3f SPOTLIGHT_DIRECTION = new Vector3f(0.25f, -1, 0);
    public static final Vector3f SPOTLIGHT_POSITION = new Vector3f(0, 2, 0);

    // Configuration for the ambient light (can only have one)
    public static final ColorRGBA AMBIENTLIGHT_COLOUR = ColorRGBA.White.mult(0.75f);

    // Configuration for the directional light (can only have one)
    public static final Vector3f DIRECTIONALLIGHT_DIRECTION = new Vector3f(1, 0, 0);
    public static final ColorRGBA DIRECTIONALLIGHT_COLOUR = ColorRGBA.Green;

    // Configuration for the point lights (can have many but sample only uses one)
    public static final Vector3f POINTLIGHT_POSITION = new Vector3f(0, 3, 0);
    public static final ColorRGBA POINTLIGHT_COLOUR = ColorRGBA.Red;
    public static final float POINTLIGHT_RADIUS = 100.0f;

    // Configuration for the camera
    public static final Vector3f CAM_LOCATION = new Vector3f(100, 45, 75);
    public static final int CAM_SPEED = 15;

    public static final int SHADOW_MAP_SIZE = 512;

    private static SpotLight spot;

    /**
     * Entry point of the program
     *
     * @param args
     */
    public static void main(String[] args) {
        BlenderModels app = new BlenderModels(); // creates an object of this class's type and...
        app.start(); // passes over control to the inherited engine method, which fires off the game loop
    }

    /**
     * The first method which is called in your game. Used to setup the state of
     * the world e.g create all your entitys in the correct position with the
     * right configuration ready to play
     */
    @Override
    public void simpleInitApp() {
        setupCamera();
        setupAAShadowFilter();
        setupLighting();
        loadModels();
        GeometryBatchFactory.optimize(rootNode); // makes all meshes the one mesh for performance (don't include sky)
        makeSky();
    }

    public void makeSky() {
        final Spatial sky = SkyFactory.createSky(assetManager, "Textures/Sky/Bright/BrightSky.dds", EnvMapType.CubeMap);
        sky.scale(350);
        rootNode.attachChild(sky);
    }

    /**
     * Creates Screen Space Ambient Occlusion filter which applies to all objects in the scene
     * and works in conjunction with the ambient lighting source to give a more realistic effect.
     */
    private void setupAAShadowFilter() {
        FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
        SSAOFilter ssaoFilter = new SSAOFilter(10f, 30, 0, 0.9f);
        fpp.addFilter(ssaoFilter);
        viewPort.addProcessor(fpp);
    }

    /**
     * sets up the camera properties to look at the scene from a distance
     */
    private void setupCamera() {
        flyCam.setMoveSpeed(CAM_SPEED);
        cam.setLocation(CAM_LOCATION);
        cam.lookAt(Vector3f.ZERO, Vector3f.UNIT_Y);
    }

    /**
     * create all the lights. This method should really be split up.
     */
    private void setupLighting() {
        Node container = new Node();
        container.addControl(new SpinnerControl());
        makeSpinner(new PointLight(POINTLIGHT_POSITION, POINTLIGHT_COLOUR, POINTLIGHT_RADIUS), container, 16);
        rootNode.addLight(spot = new SpotLight(SPOTLIGHT_POSITION, SPOTLIGHT_DIRECTION, SPOTLIGHT_RANGE, SPOTLIGHT_COLOUR, SPOTLIGHT_INNER_ANGLE * FastMath.DEG_TO_RAD, SPOTLIGHT_OUTER_ANGLE * FastMath.DEG_TO_RAD));
        rootNode.addLight(new AmbientLight(AMBIENTLIGHT_COLOUR));
        final DirectionalLight directionalLight = new DirectionalLight(DIRECTIONALLIGHT_DIRECTION, DIRECTIONALLIGHT_COLOUR);
        rootNode.addLight(directionalLight);
    }

    /**
     * Create a bunch of cubes and a really big cube for the floor
     */
    private void loadModels() {
        for (int x = -5; x < 5; x++) {
            for (int z = -5; z < 5; z++) {
                final Spatial model = assetManager.loadModel("Models/WoodCube.j3o");
                model.setLocalTranslation(x * 10, x * z % 10, z * 10);
                TangentBinormalGenerator.generate(model);
                rootNode.attachChild(model);
            }
        }
        final Spatial model2 = assetManager.loadModel("Models/WoodCube.j3o");
        model2.scale(75);
        model2.setLocalTranslation(0, -85, 0);
        TangentBinormalGenerator.generate(model2);
        rootNode.attachChild(model2);
        
        rootNode.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
    }

    /**
     * Just update the position and location of the spotlight to follow the
     * camera
     *
     * @param tpf
     */
    @Override
    public void simpleUpdate(float tpf) {
        super.simpleUpdate(tpf);
        spot.setPosition(cam.getLocation());
        spot.setDirection(cam.getDirection());
    }

    /**
     * The spinner is just a container to spin a point light around the center
     *
     * @param pointLight the light to attach (created outside this method)
     * @param container the container to attach the spinner to
     * @param distance the distance at which the ball will orbit the container
     */
    private void makeSpinner(PointLight pointLight, Node container, float distance) {
        Node light = new Node();
        light.setLocalTranslation(new Vector3f(distance, 25, 0)); // light needs to be outside the mesh or won't shine
        container.attachChild(light);
        light.addControl(new LightControl(pointLight));
        rootNode.attachChild(container);
        rootNode.addLight(pointLight);

        // create a shadow renderer for each light so they cast shadows independently.
        PointLightShadowRenderer plsr = new PointLightShadowRenderer(assetManager, SHADOW_MAP_SIZE);
        plsr.setEdgeFilteringMode(EdgeFilteringMode.PCFPOISSON);
        plsr.setLight(pointLight);
        plsr.setShadowIntensity(0.5f);
        viewPort.addProcessor(plsr);
    }

    /**
     * The SpinnerControl tells the spatial to rotate around the Y axis in a circle.
     */
    private static class SpinnerControl extends AbstractControl {

        @Override
        protected void controlUpdate(float tpf) {
            spatial.rotate(0, tpf / 4, 0);
        }

        @Override
        protected void controlRender(RenderManager rm, ViewPort vp) {
        }

    }

}
