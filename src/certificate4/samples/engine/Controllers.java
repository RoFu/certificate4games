package certificate4.samples.engine;

import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.shape.Box;

/**
 * Make a simple moving box.
 *
 * @author Roland Fuller
 */
public class Controllers extends SimpleApplication {
    
    private static class CubeControl extends AbstractControl {
        
        private static final float SPEED = 1f;
        
        @Override
        protected void controlUpdate(float tpf) {
            spatial.move(tpf * SPEED, 0, 0);            
        }
        
        @Override
        protected void controlRender(RenderManager rm, ViewPort vp) {
        }
    }
    
    private Geometry geom;
    
    public static void main(String[] args) {
        Controllers app = new Controllers();
        app.start();
    }
    
    @Override
    public void simpleInitApp() {
        makeBox();
    }
    
    public void makeBox() {
        Box b = new Box(0.5f, 0.5f, 0.5f);
        geom = new Geometry("Box", b);
        
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Green);
        geom.setMaterial(mat);
        geom.addControl(new CubeControl());
        
        rootNode.attachChild(geom);
    }
    
}
