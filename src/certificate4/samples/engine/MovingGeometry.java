package certificate4.samples.engine;

import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;

/**
 * Make a simple moving box.
 *
 * @author Roland Fuller
 */
public class MovingGeometry extends SimpleApplication {

    private static final float SPEED = 1f;

    private Geometry geom1;
    private Geometry geom2;

    public static void main(String[] args) {
        MovingGeometry app = new MovingGeometry();
        app.start();
    }

    @Override
    public void simpleInitApp() {
        geom1 = makeBox();
        geom2 = makeBox();
    }

    public Geometry makeBox() {
        Box b = new Box(0.5f, 0.5f, 0.5f);
        Geometry geom = new Geometry("Box", b);

        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Green);
        geom.setMaterial(mat);

        rootNode.attachChild(geom);
        return geom;
    }

    /**
     * During the update, the geometry we created is just moved along the x axis
     *
     * @param tpf time per frame. How many seconds have passed since the last
     * time we were in here.
     */
    @Override
    public void simpleUpdate(float tpf) {
        super.simpleUpdate(tpf);
        geom1.move(tpf * SPEED, 0, 0); // note that multiplying tpf by a fixed value gives us a guaranteed constant movement at the specified constant value
        geom2.move(tpf * -SPEED, 0, 0); // note that multiplying tpf by a fixed value gives us a guaranteed constant movement at the specified constant value

    }

}
