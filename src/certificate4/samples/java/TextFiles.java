package certificate4.samples.java;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Roland Fuller
 */
public class TextFiles {

    private final String fileName;
    private static final String FILE_EXTENSION = ".save";
    private static final String SEPARATOR = ",";

    public static void main(String[] args) {
        TextFiles textFiles = new TextFiles("test");
        textFiles.save("Here is some data, here's some more");
        textFiles.load();
    }

    public TextFiles(String fileName) {
        this.fileName = fileName;
    }

    /**
     * This will overwrite the data in the file specified
     *
     * @param data the data to write
     */
    public void save(String data) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(new File(fileName + FILE_EXTENSION)));
            writer.write(data);
            writer.close();
        } catch (IOException ex) {
            System.out.println("Input Output exception occurred. Probably the file is read only.");
        }
    }

    public void load() {
        try {
            String line;
            BufferedReader reader = new BufferedReader(new FileReader(new File(fileName + FILE_EXTENSION)));
            while ((line = reader.readLine()) != null) {
                String[] split = line.split(SEPARATOR);
                System.out.println("==========Starting to read a line/record from file==========");
                for (String element : split) {
                    System.out.println("The next element is: " + element + ". You need to do something with this data! Ideally put it in an object to return");
                }
                System.out.println("==========End of record==========");
            }
        } catch (IOException ex) {
            System.out.println("Input Output exception occurred. Probably the file doesn't exist.");
        }
    }

}