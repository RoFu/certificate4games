package certificate4.samples.java;

/**
 * An introduction to the basics of programming - variables, expressions,
 * statements.
 *
 * @author Roland Fuller
 */
public class Variables {

    public static final int REALLY_IMPORTANT_NUMBER = 8; // What's this? Why is it special?
    private int classInstanceVariable; // here is a declaration of a variable which belongs to the Variables class instances
    private static int staticClassVariable; // here is a declaration of a variable which belongs to the Variables class
    private static byte aByteVariable;

    /**
     * Entry point of the program
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {

        /*  Variable declarations - here we simply state that some variables should be created.
         You need to specify the type of data it stores followed by the name of the variable
         The variable should be named using lowerCamelCase, have no spaces or other funny characters
         (at least not at the beginning) and be named well to describe what it is used for.
         The types of variables shown here are all the fundamental types in Java. These are collectively
         called the value type variables.*/
        // 8 bit signed integer stores from -128 to 127.
        char aCharacterVariable; // 16 bit unsigned integer stores from 0 to 65,535 (unicode character)
        short aShortVariable; // 16 bit signed integer stores from -32,768 to 32,767
        int aIntegerVariable; // 32 bit signed integer -2^31 to 2^31-1
        long aLongVariable; // 64 bit signed integer stores from -2^63 to 2^63-1

        float aFloatVariable; // 32 bit floating point number (has decimal places)
        double aDoubleVariable; // 64 bit floating point number (has decimal places)

        boolean aBooleanVariable; // a single bit flag which can be true or false 

        // Strings are a special type of variable, known as a "reference type". We'll learn the details of these when we learn about object orientation
        String aStringVariable = "Strings hold text, literals must be enclosed in double quotes";

        aByteVariable = 6; // this is called "assignation" meaning assigning a value to a variable. From here on in, the variable will hold this number until it's assigned again.
        System.out.println("Here's the value of the variable" + aByteVariable); // the print line method is built-in to Java library code. It displays the value you place in the brakets () on the console.
        // Here we are "adding" a string literal to the variable *contents*. This is called string concatenation.
        aBooleanVariable = true;
        aBooleanVariable = false;

        aCharacterVariable = 'Q'; // character literals must be enclosed in single quotes.

        aByteVariable = (byte) 128; // we have to employ a type cast here to explicitly force the 128 value to fit into a byte variable (it still won't though, it will overflow)

        aIntegerVariable = 50; // variable now holds 50
        aIntegerVariable = aIntegerVariable * 2;

        aShortVariable = 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10;
        aShortVariable += 5;
        aShortVariable *= 2;

        aDoubleVariable = 5f;
        aDoubleVariable++;
        aDoubleVariable--;
        float anotherFloat = 5f; // when using literal data, you may need a suffix to let the compiler know 
        float yetAnotherFloat = (float) aDoubleVariable; // we can cast down explicitly if the types are compatible with understanding we might lose data

        // All of the preceeding lines of code have been statements, which are set of expressions and instructions
        // that change the state of the program (e.g change the value stored in a variable)
        // the following line isn't a statement, just an expression. The compiler will complain as the expression needs to result in a functional statement.
        // (5 + 6);
        // It evaluates this expressino to 11, but then as there is no instruction as to what to do with the result, it is discarded and no change to the program's state occurs. 
        boolean result = aByteVariable >= 3;
        result = true && false;
        result = true || false;
        result = !false;
    }
}
