package certificate4.samples.java;

/**
 * Simple demonstration of extracting command line parameters and building a simple array.
 *
 * @author Roland Fuller
 */
public class Arrays {

    /**
     *
     * @param args command line parameters
     */
    public static void main(String[] args) {
        System.out.println(args[0]);
        System.out.println(args[1]);

        String myStrings[] = new String[3];
        myStrings[0] = "good-bye";
        myStrings[1] = "cruel";
        myStrings[2] = args[1];
        System.out.println(myStrings[0]);
        System.out.println(myStrings[1]);
        System.out.println(myStrings[2]);
    }

}
