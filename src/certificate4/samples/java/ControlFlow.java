package certificate4.samples.java;

/**
 * Examples of control flow techniques.
 *
 * @author Roland Fuller
 */
public class ControlFlow {

    /**
     * Entry point of the program
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {

        int score = 5;
        int scoreToWin = 10;

        // The if/else if/else conditional construct - first example
        if (score >= scoreToWin) {
            System.out.println("You won!");
        } else if (score >= scoreToWin / 2) {
            System.out.println("You're half way to winning!");
        } else if (score < 0) {
            System.out.println("Better start trying!");
        } else {
            System.out.println("Ouch!");
        }

        // A series of if statements - second example
        if (score >= scoreToWin) {
            System.out.println("You won!");
        }
        if (score >= scoreToWin / 2) {
            System.out.println("You're half way to winning!");
        }
        if (score < 0) {
            System.out.println("Better start trying!");
        } else {
            System.out.println("Ouch!");
        }

        // The "ternary" operator
        String message = score >= scoreToWin ? "You have won!" : "You have not won yet.";
        System.out.println(message);

        // The switch case
        switch (score) {
            case 1:
            case 2:
            case 3:
            case 4:
                System.out.println("Ouch!");
                break;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                System.out.println("You're half way to winning!");
                break;
            case 10:
                System.out.println("You won!");
                break;
            default:
                System.out.println("Ouch!");
        }
    }
}
