package certificate4.samples.java;

/**
 * This is a demonstration of methods
 *
 * @author Roland Fuller
 */
public class Methods {

    private static int staticCounter;
    private int objectCounter;

    /**
     * main is a required method
     *
     * @param args command line parameters
     */
    public static void main(String[] args) {
        int result;
        result = Methods.staticMethod(); // call the method from the class name without creating an instance. Put the return value into the variable
        System.out.println("The static method returned: " + result); // display the result (from the middle-man variable)
        Methods.staticMethod(); // run the method again, but discard the result
        System.out.println("The static variable is now: " + staticCounter); // display the static variable, directly

        Methods methods = new Methods();// Here we create an object instance from the Methods class
        result = methods.instanceMethod();// from the reference we can access the method in the object
        System.out.println("The result returned from the instance method: " + result); // display the result (from the middle-man variable)
        methods.staticMethod(); // technically we can do this but it is less clear. Better to use the class name.
    }

    /**
     * A static method belongs to the class it's in and can be accessed directly from the class without having to create
     * an instance of an object.
     */
    private static int staticMethod() {
        staticCounter++;
        return staticCounter;
    }

    /**
     * An instance method belongs to an object and can only be accessed through an object reference.
     */
    private int instanceMethod() {
        objectCounter++;
        return objectCounter;
    }
}
