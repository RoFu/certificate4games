package certificate4.samples.java.objectorientation.entity;

/**
 * Things which can be damaged should implement this interface. This gives a consistent way to damage all kinds of
 * objects. The simple integer parameter could be reified into a full blown object to represent different kinds of
 * damage.
 *
 * @author Roland Fuller
 */
public interface Damagable {

    void inflict(int amount);

}
