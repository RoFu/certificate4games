package certificate4.samples.java.objectorientation.entity;

import certificate4.samples.java.objectorientation.character.Character;

/**
 * What effect an item can have on a target character
 *
 * @author Roland Fuller
 */
public interface Effect {

    void apply(Character user, Character target);

}
