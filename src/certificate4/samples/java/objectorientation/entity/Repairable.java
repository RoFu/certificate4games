package certificate4.samples.java.objectorientation.entity;

/**
 * Something which may have a repair action placed on it, can also be broken so as to require repair.
 *
 * @author Roland Fuller
 */
public interface Repairable {

    boolean isBroken();

    void repair();
}
