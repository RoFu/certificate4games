package certificate4.samples.java.objectorientation.items;

import certificate4.samples.java.objectorientation.character.Character;
import certificate4.samples.java.objectorientation.entity.Effect;
import java.util.LinkedList;
import java.util.List;

/**
 * An abstract class demonstrating shared attributes and an inheritance hierarchy. This class is really just a container
 * and is already showing signs of moving on to a more powerful creator pattern like builder, especially as specific
 * item types profilerate.
 *
 * @author Roland Fuller
 */
public abstract class Item {

    private Character owner;
    private final String name; // immutable name attribute
    private final int value; // immutable value attribute
    private final int weight; // immutable weight attribute
    private final List<Effect> effects; // what effects the item has

    /**
     * Overloaded constructor method
     *
     * @param name the name of the item
     * @param value how much it's worth
     * @param weight how heavy it is
     */
    public Item(String name, int value, int weight) {
        this.name = name;
        this.value = value;
        this.weight = weight;
        effects = new LinkedList<>();
    }

    /**
     * Core Constructor method
     *
     * @param owner who owns this item
     * @param name the name of the item
     * @param value how much it's worth
     * @param weight how heavy it is
     */
    public Item(Character owner, String name, int value, int weight) {
        this(name, value, weight);
        setOwner(owner);
    }

    /**
     * Apply all the effects to the character
     *
     * @param target to apply to
     */
    public void use(Character user, Character target) {
        for (Effect effect : getEffects()) {
            effect.apply(user, target);
        }
    }

    /**
     * Add a new effect to this item
     *
     * @param effect
     */
    protected final void addEffect(Effect effect) {
        effects.add(effect);
    }

    /**
     *
     * @return the list of effects this item has
     */
    protected List<Effect> getEffects() {
        return effects;
    }

    /**
     * @param character set who owns this item as this can change
     */
    public void setOwner(Character character) {
        this.owner = character;
    }

    /**
     * @return the owner
     */
    public Character getOwner() {
        return owner;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the value
     */
    public int getValue() {
        return value;
    }

    /**
     * @return the weight
     */
    public int getWeight() {
        return weight;
    }
}
