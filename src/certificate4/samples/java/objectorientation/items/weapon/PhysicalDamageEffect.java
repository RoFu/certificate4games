package certificate4.samples.java.objectorientation.items.weapon;

import certificate4.samples.java.objectorientation.character.Character;
import certificate4.samples.java.objectorientation.entity.Effect;

/**
 * An effect which will inflict damage to a target.
 *
 * @author Roland Fuller
 */
public class PhysicalDamageEffect implements Effect {

    private final int damage; // how much damage to do.

    /**
     * Construct
     *
     * @param damage
     */
    public PhysicalDamageEffect(int damage) {
        this.damage = damage;
    }

    /**
     * Apply the damage to a target
     *
     * @param user
     * @param target
     */
    @Override
    public void apply(Character user, Character target) {
        target.inflict(damage);
    }

}
