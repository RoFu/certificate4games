package certificate4.samples.java.objectorientation.items.weapon;

import certificate4.samples.java.objectorientation.character.Character;
import certificate4.samples.java.objectorientation.entity.Effect;

/**
 * Currently only support the "stupify" spell....
 *
 * @author Roland Fuller
 */
public class MagicDamageEffect implements Effect {

    private final int magicDamage;

    /**
     * Constructor
     *
     * @param magicDamage how much damage to do
     */
    public MagicDamageEffect(int magicDamage) {
        this.magicDamage = magicDamage;
    }

    /**
     * When this effect is used, it makes the target a little dumber
     *
     * @param user who is using the spell
     * @param target the afflicted
     */
    @Override
    public void apply(Character user, Character target) {
        // This isn't very defensive since we are assuming the character has an intelligence stat to begin with... this may be a fatal assumption.
        // (it's safe because we were defensive in the Character class :) )
        target.setStatistic(Character.STATISTIC_INTELLIGENCE, user.getStatistic(Character.STATISTIC_INTELLIGENCE) - magicDamage);
    }
}
