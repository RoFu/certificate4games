package certificate4.samples.java.objectorientation.items.weapon;

import certificate4.samples.java.objectorientation.character.Character;
import certificate4.samples.java.objectorientation.items.BreakableItem;

/**
 *
 * @author Roland Fuller
 */
public class Sword extends BreakableItem {

    public static final String SWORD_NAME = "sword";
    public static final int SWORD_DURABILITY = 15;
    public static final int SWORD_WEIGHT = 6;
    public static final int SWORD_VALUE = 5;
    public static final int SWORD_DAMAGE = 43;

    /**
     * Sword constructor
     *
     * @param character who carries it
     */
    public Sword(Character character) {
        super(character, SWORD_NAME, SWORD_VALUE, SWORD_WEIGHT, SWORD_DURABILITY);
        addEffect(new PhysicalDamageEffect(SWORD_DAMAGE));
    }
}
