package certificate4.samples.java.objectorientation.items.weapon;

import certificate4.samples.java.objectorientation.character.Character;
import certificate4.samples.java.objectorientation.items.BreakableItem;

/**
 *
 * @author Roland Fuller
 */
public class Axe extends BreakableItem {

    public static final String AXE_NAME = "axe";
    public static final int AXE_DURABILITY = 15;
    public static final int AXE_WEIGHT = 6;
    public static final int AXE_VALUE = 5;
    public static final int AXE_DAMAGE = 50;

    /**
     * Constructor sets up axe object
     *
     * @param character
     */
    public Axe(Character character) {
        super(character, AXE_NAME, AXE_VALUE, AXE_WEIGHT, AXE_DURABILITY);
        addEffect(new PhysicalDamageEffect(AXE_DAMAGE));
    }

}
