package certificate4.samples.java.objectorientation.items.weapon;

import certificate4.samples.java.objectorientation.character.Character;
import certificate4.samples.java.objectorientation.items.BreakableItem;

/**
 *
 * @author Roland Fuller
 */
public class Wand extends BreakableItem {

    public static final String WAND_NAME = "wand";
    public static final int WAND_DURABILITY = 5;
    public static final int WAND_WEIGHT = 1;
    public static final int WAND_VALUE = 100;
    public static final int MAGIC_EFFECT = 1;
    private int charge;
    private final int maxCharge;

    /**
     * Construct a wand
     *
     * @param character the owner
     * @param charges how many charges the wand has - and also how many it can hold when depleted/recharged
     */
    public Wand(Character character, int charges) {
        super(character, WAND_NAME, WAND_VALUE, WAND_WEIGHT, WAND_DURABILITY);
        this.charge = maxCharge = charges;
        addEffect(new MagicDamageEffect(MAGIC_EFFECT));
    }

    /**
     * Replenish the charges up to max.
     */
    public void recharge() {
        this.charge = maxCharge;
    }

    /**
     * Further guard the use of a wand weapon with the charge count.
     *
     * @param owner who wields the wand
     * @param target the target of the use
     */
    @Override
    public void use(Character owner, Character target) {
        if (charge > 0) {
            super.use(owner, target);
        }
    }
}
