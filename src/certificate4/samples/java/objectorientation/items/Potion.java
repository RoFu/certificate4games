package certificate4.samples.java.objectorientation.items;

import certificate4.samples.java.objectorientation.character.Character;
import static certificate4.samples.java.objectorientation.character.Character.STATISTIC_HEALTH;

/**
 *
 * @author Roland Fuller
 */
public class Potion extends Item {

    private static final int HEALTH = 10;
    public static final String POTION_NAME = "magic potion";
    public static final int POTION_WEIGHT = 1;
    public static final int POTION_VALUE = 100;

    /**
     * Construct a magical potion - yum
     *
     * @param character who owns it - only they can use it
     */
    public Potion(Character character) {
        super(POTION_NAME, POTION_VALUE, POTION_WEIGHT);
    }

    /**
     * When I drink the potion
     *
     * @param user must be the target
     * @param target must be the user
     */
    @Override
    public void use(Character user, Character target) {
        if (user == target) // you can lead but can't make them drink
        {
        super.use(user, target);
            user.setStatistic(STATISTIC_HEALTH, user.getStatistic(STATISTIC_HEALTH) + HEALTH);
        }
    }
}
