package certificate4.samples.java.objectorientation.items;

import certificate4.samples.java.objectorientation.character.Character;
import certificate4.samples.java.objectorientation.entity.Damagable;
import certificate4.samples.java.objectorientation.entity.Repairable;
import java.util.Random;

/**
 * An item which can recieve damage and fail to work when broken, but be repaired.
 *
 * @author Roland Fuller
 */
public abstract class BreakableItem extends Item implements Damagable, Repairable {

    private static final float BREAKABILITY = 0.99f; // constant for all items but could be parameterized.
    private final int maxDurability;
    private int durability;

    /**
     * Constructor method for all breakables
     *
     * @param owner who owns it
     * @param name the name of the item
     * @param value how much it's worth
     * @param weight how heavy it is to carry
     */
    public BreakableItem(Character owner, String name, int value, int weight, int durability) {
        super(owner, name, value, weight);
        this.durability = maxDurability = durability;
        repair();
    }



    /**
     * Confirm to use if the item is working or not
     *
     * @return true if it is broken (not working) false otherwise
     */
    @Override
    public boolean isBroken() {
        return durability <= 0;
    }
    /**
     * Break the item a little bit
     *
     * @param damage how much
     */
    @Override
    public void inflict(int damage) {
        durability -= damage;
    }
    /**
     * Reset the durability.
     */
    @Override
    public void repair() {
        durability = maxDurability;
    }

    /**
     * It polymorphically restricts usage to when it is in good repair.
     *
     * @param target
     */
    @Override
    public void use(Character owner, Character target) {
        if (!isBroken()) {
            super.use(getOwner(), target);
            if (new Random().nextFloat() > BREAKABILITY) {
                inflict(1);
            }
        }
    }
}
