package certificate4.samples.java.objectorientation.character;

import certificate4.samples.java.objectorientation.entity.Damagable;
import certificate4.samples.java.objectorientation.items.Item;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * A class which represents any character in the game. This class demonstrates composition, specifically in the
 * statistics, inventory and weapon.
 *
 * @author Roland Fuller
 */
public class Character implements Damagable {

    /**
     * Define some constants useful to all characters.
     */
    public static final int STATISTICS_INITAL_SIZE = 10;
    public static final String STATISTIC_HEALTH = "health";
    public static final String STATISTIC_STRENGTH = "strength";
    public static final String STATISTIC_INTELLIGENCE = "intelligence";
    public static final String STATISTIC_MAGIC = "magic";
    public static int count; // a counter for the number of character instances
    private final String name; // the name of the character
    private final Map<String, Integer> statistics; // a map of statistics the character has
    private final List<Item> inventory; // a collection of items the character is carrying

    /**
     * Constructor method
     *
     * @param name the name of the character, which cannot be changed once set (immutable attribute)
     */
    public Character(String name) {
        this.name = name; // set the name. Note the this reference to current object to resolve name difference
        this.statistics = new HashMap<>(STATISTICS_INITAL_SIZE); // set up collection object
        this.inventory = new LinkedList<>(); // set up collection object
        count++; // increment the instance count
    }

    /**
     * A static method - belonging to the class, not the instance.
     *
     * @return the number of instances from the characters static variable.
     */
    public static int getCount() {
        return count;
    }

    /**
     * Adding or modifying a character's statistic
     *
     * @param name the name of the statistic to set
     * @param value the value to set it to
     */
    public void setStatistic(String name, int value) {
        statistics.put(name, value);
    }

    /**
     * Retrieve a specific stat
     *
     * @param name the name of the stat to retrieve
     * @return the value of the stat
     */
    public int getStatistic(String name) {
        if (statistics.containsKey(name)) {
            return statistics.get(name);
        } else {
            return 0;
        }
    }

    /**
     * Give the character an item object to their inventory
     *
     * @param item the item to be given
     */
    public void giveItem(Item item) {
        if (inventoryWeight() <= getStatistic(STATISTIC_STRENGTH)) {
            inventory.add(item);
        }
    }

    /**
     * Sum up all the items we're carrying
     *
     * @return the weight total
     */
    private int inventoryWeight() {
        int totalWeight = 0;
        for (Item item : inventory) {
            totalWeight += item.getWeight();
        }
        return totalWeight;
    }

    /**
     * Access the whole list of items in the character's inventory
     *
     * @return the list of item objects
     */
    public List<Item> getItems() {
        return inventory;
    }

    /**
     * A request for an item reference if the character is holding something by this name
     *
     * @param itemName the name of the item we're looking for
     * @return the item object, if being held
     */
    public Item getItemNamed(String itemName) {
        for (Item item : inventory) {
            if (item.getName().equals(itemName)) {
                return item;
            }
        }
        return null;
    }

    /**
     * Use an item on a target character, if I have it
     *
     * @param item the item to use
     * @param character the character to target
     */
    public void useItemOn(Item item, Character character) {
        int index = inventory.indexOf(item);
        if (index > -1) {
            inventory.get(index).use(this, character);
        }
    }

    /**
     * Use the item specified on myself
     *
     * @param item the item to use
     */
    public void useItem(Item item) {
        useItemOn(item, this);
    }

    /**
     * Overriding/implementing the takeDamage method of the Damageable interface i.e characters can be hurt by an
     * attack.
     *
     * @param damage the amount of damage to take
     */
    @Override
    public void inflict(int damage) {
        if (statistics.containsKey(STATISTIC_HEALTH)) {
            setStatistic(STATISTIC_HEALTH, getStatistic(STATISTIC_HEALTH) - damage);
        }
    }
}
