package certificate4.samples.java.objectorientation;

import certificate4.samples.java.objectorientation.character.Character;
import certificate4.samples.java.objectorientation.creation.Factory;
import certificate4.samples.java.objectorientation.items.Potion;
import certificate4.samples.java.objectorientation.items.weapon.Axe;
import certificate4.samples.java.objectorientation.items.weapon.Sword;
import certificate4.samples.java.objectorientation.items.weapon.Wand;

/**
 * This is simply a test driver at this stage.
 *
 * @author Roland Fuller
 */
public class Main {

    /**
     * Entry point of the program
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        Character troll = Factory.makeTroll();
        Character barbarian = Factory.makeBarbarian();
        Character sorcerer = Factory.makeSorcerer();
        System.out.println("There are: " + Character.getCount()+ " characters");

        troll.useItemOn(troll.getItemNamed(Wand.WAND_NAME), barbarian);
        barbarian.useItemOn(barbarian.getItemNamed(Sword.SWORD_NAME), troll);
        sorcerer.useItemOn(sorcerer.getItemNamed(Wand.WAND_NAME), troll);
        troll.useItemOn(troll.getItemNamed(Potion.POTION_NAME), troll);
    }
}
