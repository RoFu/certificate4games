package certificate4.samples.java.objectorientation.creation;

import certificate4.samples.java.objectorientation.character.Character;
import static certificate4.samples.java.objectorientation.character.Character.STATISTIC_HEALTH;
import static certificate4.samples.java.objectorientation.character.Character.STATISTIC_INTELLIGENCE;
import static certificate4.samples.java.objectorientation.character.Character.STATISTIC_MAGIC;
import static certificate4.samples.java.objectorientation.character.Character.STATISTIC_STRENGTH;
import certificate4.samples.java.objectorientation.items.Potion;
import certificate4.samples.java.objectorientation.items.weapon.Axe;
import certificate4.samples.java.objectorientation.items.weapon.Sword;
import certificate4.samples.java.objectorientation.items.weapon.Wand;

/**
 * A factory method pattern class used to instantiate different types of characters. This is a kind of creator pattern.
 *
 * @author Roland Fuller
 */
public class Factory {

    private static final String MONSTER_TROLL = "troll";
    public static final int TROLL_STRENGTH = 10;
    public static final int TROLL_MAGIC = 1;
    public static final int TROLL_HEALTH = 100;
    private static final String BARBARIAN_NAME = "barbarian";
    public static final int BARBARIAN_STRENGTH = 25;
    public static final int BARBARIAN_HEALTH = 150;
    private static final String SORCERER_NAME = "sorcerer";
    public static final int SORCERER_MAGIC = 125;
    public static final int SORCERER_INTELLIGENCE = 10;
    public static final int SORCERER_HEALTH = 55;

    /**
     * Private so that the factory class itself cannot be instantiated, as all the methods are statically accessible.
     */
    private Factory() {
    }

    /**
     * Create a character and set it up as a Troll type.
     *
     * @return a Troll character
     */
    public static Character makeTroll() {
        Character character = new Character(MONSTER_TROLL);
        character.setStatistic(STATISTIC_HEALTH, TROLL_HEALTH);
        character.setStatistic(STATISTIC_MAGIC, TROLL_MAGIC);
        character.setStatistic(STATISTIC_STRENGTH, TROLL_STRENGTH);
        character.giveItem(new Axe(character));
        character.giveItem(new Potion(character));
        return character;
    }

    /**
     * Create a character and set it up as a Barbarian type.
     *
     * @return a Barbarian character
     */
    public static Character makeBarbarian() {
        Character character = new Character(BARBARIAN_NAME);
        character.setStatistic(STATISTIC_HEALTH, BARBARIAN_HEALTH);
        character.setStatistic(STATISTIC_STRENGTH, BARBARIAN_STRENGTH);
        character.giveItem(new Sword(character));
        return character;
    }

    /**
     * Create a character and set it up as a Sorcerer type.
     *
     * @return a Sorcerer character
     */
    public static Character makeSorcerer() {
        Character character = new Character(SORCERER_NAME);
        character.setStatistic(STATISTIC_HEALTH, SORCERER_HEALTH);
        character.setStatistic(STATISTIC_INTELLIGENCE, SORCERER_INTELLIGENCE);
        character.setStatistic(STATISTIC_MAGIC, SORCERER_MAGIC);
        character.giveItem(new Wand(character, 10));
        character.giveItem(new Potion(character));
        return character;
    }
}
