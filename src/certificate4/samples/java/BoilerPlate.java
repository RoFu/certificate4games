package certificate4.samples.java;

/**
 * This is the most basic program which will compile and execute.
 * 
 * @author Roland Fuller
 */
public class BoilerPlate {

    /**
     *
     * @param args command line parameters
     */
    public static void main(String[] args) {
    }

}
