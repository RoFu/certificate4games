package certificate4.samples.java;

/**
 * A demonstration of the different iteration constructs
 *
 * @author Roland Fuller
 */
public class Iteration {

    /**
     * Entry point of the program
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {

        // old school for loop - to perform a specific, known number of iterations with fixed pattern
        for (int i = 0; i < 10; i++) {
            System.out.println("In the for loop, the value of i is now: " + i);
        }

        // new school for loop - to step through a data structure (like an array)
        for (String arg : args) {
            System.out.println("In the new for loop, the next string in the array is: " + arg);
        }

        int i = 0;
        // while loop - to perform an action until a condition is true
        // note that the condition is evaluated before entering the loop so therefore the code inside
        // may or may not be run at all.
        while (i < 10) {
            System.out.println("In the while loop, the value of i is now: " + i);
            i++;
        }

        // do-while loop. Similar to while loop but it guarantees the code will run a minimum of once
        // because the condition is evaluated at the end of the block.
        do {
            System.out.println("In the do-while loop, the value of i is now: " + i);
        } while (i < 10);

    }
}
