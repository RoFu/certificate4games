package certificate4.samples.java;

enum MyEnumeration {
    BAD, OK, GOOD
};

/**
 * A straightforward demonstration of the enumeration data type.
 *
 * @author Roland Fuller
 */
public class Enumerations {

    /**
     * Entry point of the program
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {

        MyEnumeration myEnumVariable = MyEnumeration.BAD; // create a variable to hold a value allowed in the enum
        System.out.println(myEnumVariable); // print it out 
        myEnumVariable = MyEnumeration.OK; // change the value to another enum constant
        System.out.println(myEnumVariable); // print it out
        // myEnumVariable = 66; // not allowed - can only contain values defined in the enum! Results in a compiler error.
    }
}
