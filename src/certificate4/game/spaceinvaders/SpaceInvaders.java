package certificate4.game.spaceinvaders;

import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Sphere;

/**
 * The SpaceInvaders class inherits from the SimpleApplication class and also: implements the AnalogListener interface
 * and the PhysicsCollisionListener interface. Thsi means that the SpaceInvaders class will contain the methods that
 * these interfaces specify. The reason is so that this class can function in the role the the interfaces are intended
 * to cover i.e listen to user keyboard input and listen to the physics engine for collision events.
 *
 * @author Roland Fuller
 */
public class SpaceInvaders extends SimpleApplication implements AnalogListener, PhysicsCollisionListener {

    // A bunch of constant variables so our code doesn't have magical numbers strewn about
    private static final float SHIP_SPEED = 0.1f; // How fast can the paddle move
    private static final float SHIP_MASS = 10f; // how heavy is the paddle (this effects collision maths with the missile)
    private static final float REFIRE_TIME = 0.5f; // how fast can the ship fire
    private static final float MISSILE_MASS = 1f; // how heavy is the missile
    private static final float MISSILE_SPEED = 75; // How fast does the missile move
    private static final float MISSILE_SIZE = 0.75f; // How big is the missile (radius)

    // Names of the game objects for identification
    private static final String SHIP_NAME = "Ship";
    private static final String MISSILE_NAME = "Missile";
    private static final String ALIEN_NAME = "Alien";
    private static final String WALL_NAME = "Wall";

    // Names of input commands
    private static final String KEY_SHOOT = "Shoot";
    private static final String KEY_LEFT = "Left";
    private static final String KEY_RIGHT = "Right";

    // The main method which just creates a SpaceInvaders object and hands over control to the engine
    public static void main(String[] args) {
        SpaceInvaders app = new SpaceInvaders();
        app.start();
    }

    // variables that we need to access throughout the program
    private BulletAppState bulletAppState; // a reference to the physics engine
    private Node ship; // the paddle objects - need to tell them to move
    private int score = 0, lives = 3;
    private BitmapText scoreText, livesText; // the text objects to display the scores
    private float refireTimer = 0;

    /**
     * simpleInitApp - runs once only, when the game starts
     */
    @Override
    public void simpleInitApp() {
        setDisplayFps(false);
        setDisplayStatView(false);
        flyCam.setEnabled(false); // turn off the default WASD FPS camera
        cam.setLocation(new Vector3f(0, 0, 150)); // move the camera away from the scene a bit
        setDisplayStatView(false); // turn off the debugging statistics display
        initPhysics(); // set up the physics engine
        initKeys(); // set up the key input
        initScores(); // set up the score GUI objects
        initGameObjects(); // create all the game objects - missile, paddles, walls, goals
    }

    /**
     * Set up the physics engine
     */
    public void initPhysics() {
        stateManager.attach(bulletAppState = new BulletAppState()); // create and attach the physics engine - there isn't one by default
        bulletAppState.getPhysicsSpace().addCollisionListener(this); // tell the physics engine that this class (Pong) wants to know when a collision happens 
        //bulletAppState.setDebugEnabled(true);
    }

    /**
     * Set up the user input keys
     */
    public void initKeys() {
        inputManager.addListener(this, new String[]{KEY_SHOOT, KEY_LEFT, KEY_RIGHT}); // Tell the input manager this class (Pong) wants to hear about the following input commands
        inputManager.addMapping(KEY_SHOOT, new KeyTrigger(KeyInput.KEY_SPACE)); // attach this input command to this particular key
        inputManager.addMapping(KEY_LEFT, new KeyTrigger(KeyInput.KEY_LEFT)); // attach this input command to this particular key
        inputManager.addMapping(KEY_RIGHT, new KeyTrigger(KeyInput.KEY_RIGHT)); // attach this input command to this particular key
    }

    /**
     * Setup the objects used to display the player's scores
     */
    protected void initScores() {
        scoreText = new BitmapText(guiFont, false); // create a bitmap text object to be shown on the GUI
        scoreText.setSize(guiFont.getCharSet().getRenderedSize() * 4); // setup the size of the font
        // set the location to be one fifth the way across the screen and one sixth down (plus font size)
        scoreText.setLocalTranslation(0, settings.getHeight() + scoreText.getLineHeight() / 6, 0);
        guiNode.attachChild(scoreText); // need to attach a bitmap object to the gui node so it can be updated/rendered by the engine

        livesText = new BitmapText(guiFont, false); // create a bitmap text object to be shown on the GUI
        livesText.setSize(guiFont.getCharSet().getRenderedSize() * 4); // setup the size of the font
        // set the location to be one fifth the way across the screen and one sixth down (plus font size)
        livesText.setLocalTranslation(0, settings.getHeight() - (livesText.getLineHeight() / 6) * 4, 0);
        guiNode.attachChild(livesText); // need to attach a bitmap object to the gui node so it can be updated/rendered by the engine
    }

    /**
     * Create all the game objects
     */
    public void initGameObjects() {
        makeWalls();
        makeAliens();
        ship = makeShip(new Vector3f(0, -55, 0)); // the paddle is created and passed back so we can store it in the variable to reference later...
    }

    /**
     * Create all the walls
     */
    public void makeWalls() {
        makeWall(new Vector3f(-55, -45, 0));
        makeWall(new Vector3f(-30, -45, 0));
        makeWall(new Vector3f(-5, -45, 0));
        makeWall(new Vector3f(20, -45, 0));
        makeWall(new Vector3f(45, -45, 0));
    }

    // create a bunch of aliens by using a nested for loop to create a 2d grid of them
    public void makeAliens() {
        int xPos = -51;
        int yPos = -15;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 5; j++) {
                makeAlien(new Vector3f(xPos + i * 12, yPos + j * 12, 0));
            }
        }
    }

    /**
     * Create a paddle object. This relies on the core makeGameObject method to set up the base object and then
     * customises it with the parameters and also extracts the physics controller to set it as a kinematic type of
     * physics object
     *
     * @param name - setup the name of the object - as either paddle needs a unique name
     * @param location - set the position of the object
     * @param colour - setup the colour of the object
     */
    public Node makeShip(Vector3f location) {
        Node shipNode = makeGameObject(SHIP_NAME, new Box(4f, 1f, 1f), location, ColorRGBA.Red, SHIP_MASS); // create the core Node object and return it
        RigidBodyControl rigidBodyControl = shipNode.getControl(RigidBodyControl.class); // extract the physics controller from the game object
        rigidBodyControl.setKinematic(true); // ...so we can set it as a kinematic object
        return shipNode; // return the game object back to the caller
    }

    /**
     * Make a missile object
     *
     * @return the completed missile object, not currently necessary but for consistency and so you could make more than
     * one missile...
     */
    public Node makeMissile(Vector3f location, Vector3f direction, float offset, ColorRGBA colour) {
        location = location.clone();
        direction = direction.clone();
        location.addLocal(direction.mult(offset)); // add an offset to the missile so it doesn't appear at the center of the shooter
        Node missileNode = makeGameObject(MISSILE_NAME, new Sphere(8, 8, MISSILE_SIZE), location, colour, MISSILE_MASS); // make the core object and return it back
        RigidBodyControl rigidBodyControl = missileNode.getControl(RigidBodyControl.class); // get access to the physics controller and store it in the missile variable for later use
        rigidBodyControl.setGravity(Vector3f.ZERO); // turn gravity off for the missile, otherwise it will fall down
        rigidBodyControl.setLinearVelocity(direction.mult(MISSILE_SPEED));
        return missileNode;
    }

    /**
     * Make a wall object by relying on the core makeGameObject method with custom parameters
     *
     * @param location - where the wall should be placed
     * @return the wall object node, not currently needed but for consistency
     */
    private void makeWall(Vector3f location) {
        // create a block of cubes with physics controllers all clumped together
        for (int i = 0; i < 15; i++) {
            Vector3f position = location.add(new Vector3f(-1 + (i * 1f), -1, 0));
            for (int j = 0; j < 5; j++) {
                makeGameObject(WALL_NAME, new Box(0.25f, 0.25f, 0.25f), position, ColorRGBA.Blue, 0);
                position.addLocal(0, 1f, 0);
            }
        }
    }

    /**
     * Make a goal wall
     *
     * @param name need this because either goal should be uniquely identified on collision
     * @param location
     * @return
     */
    public Node makeAlien(Vector3f location) {
        Node alien = makeGameObject(ALIEN_NAME, new Box(3, 3, 1), location, ColorRGBA.Green, 1);
        RigidBodyControl rigidBodyControl = alien.getControl(RigidBodyControl.class); // get access to the physics controller and store it in the missile variable for later use
        rigidBodyControl.setGravity(Vector3f.ZERO); // turn gravity off for the missile, otherwise it will fall down
        alien.addControl(new AlienControl(this));
        return alien;
    }

    /**
     * This is the core method for creating any game object. It contains all the common ground that is required for
     * making any type of object, and takes in a bunch of parameters to help customise it to be different
     *
     * @param name setup the name of the object so we can identify it on collisions etc
     * @param mesh provide the model for the object (so this method is agnostic about how it looks)
     * @param location place to put the object
     * @param colour setup a colour for the material which will be applied to the model (we don't do texturing etc yet)
     * @param mass how heavy to make the physics controller of the object - effects its behaviour on collisions
     * @return
     */
    public Node makeGameObject(String name, Mesh mesh, Vector3f location, ColorRGBA colour, float mass) {
        Node node = new Node(name); // create a node object which is a container for all the components of the game object
        node.setLocalTranslation(location); // move the node objec to the desired location
        Geometry geometry = new Geometry(node.getName(), mesh); // create a geometry object that will be rendered with the model provided
        Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md"); // create a material (skin) for the model
        material.setColor("Color", colour); // set the colour of the material to the one provided
        material.preload(renderManager);
        geometry.setMaterial(material); // apply the material to the model
        RigidBodyControl rigidBodyControl = new RigidBodyControl(mass); // create a physics controller for the game object
        node.attachChild(geometry); // attach the model to the node container object
        node.addControl(rigidBodyControl); // attach the physics controller to the node container
        bulletAppState.getPhysicsSpace().add(rigidBodyControl); // add the physics controller to the physics engine so its movement can be managed by the engine
        rootNode.attachChild(node); // add the node object to the "scene graph" so they can be rendered and updated.
        return node; // return the completed object back to the caller so it can do further stuff with it
    }

    /**
     * This is the method implemented by the PhysicsCollisionListener interface which is automatically called by the
     * physics engine any time two physics objects touch
     *
     * @param event
     */
    @Override
    public void collision(PhysicsCollisionEvent event) {
        // We use our resolve method to see if either node A or node B involved in the collision was a known object
        Spatial alien = resolve(ALIEN_NAME, event.getNodeA(), event.getNodeB());
        Spatial ship = resolve(SHIP_NAME, event.getNodeA(), event.getNodeB());
        Spatial missile = resolve(MISSILE_NAME, event.getNodeA(), event.getNodeB());
        Spatial wall = resolve(WALL_NAME, event.getNodeA(), event.getNodeB());

        // Once resolved, we can see which objects we are dealing with.
        // Everything is in a nested if, because we only care about collisions with the missile and (other thing)
        if (missile != null) {
            if (alien != null) { // player one's goal touched the missile
                score++; // increment player two's score
                removeGameObject(alien);
            } else if (ship != null) { // player one's goal touched the missile
                lives--;
            } else if (wall != null) { // player one's goal touched the missile
                removeGameObject(wall);
            }
            removeGameObject(missile);
        }
    }

    private void removeGameObject(Spatial missile) {
        bulletAppState.getPhysicsSpace().removeAll(missile);
        missile.removeFromParent();
    }

    /**
     * A simple utility method which checks to see which of two nodes has the name we're looking for
     *
     * @param name The name we're testing for
     * @param nodeA the first node involved in the collision
     * @param nodeB the second node involved in the collision
     * @return the node that had the name we were looking for
     */
    private Spatial resolve(String name, Spatial nodeA, Spatial nodeB) {
        if (nodeA.getName().equals(name)) {
            return nodeA;
        } else if (nodeB.getName().equals(name)) {
            return nodeB;
        }
        return null;
    }

    /**
     * Hook into the update loop of the engine
     *
     * @param tpf
     */
    @Override
    public void simpleUpdate(float tpf) {
        // change the bitmapText objects to be whatever the score currently is.
        scoreText.setText("score: " + score);
        livesText.setText("lives: " + lives);
        refireTimer += tpf;
    }

    /**
     * The method implemented from the AnalogListener interface which allows us to respond to input manager when it
     * tells us that some key was pressed
     *
     * @param name the name of the command that occurred
     * @param value unused - the amount the key was pressed (only makes sense for thumbsticks etc, not keyboard)
     * @param tpf amount of time elapsed - unused
     */
    @Override
    public void onAnalog(String name, float value, float tpf) {
        // Switch on the name of the command that happened
        switch (name) {
            case KEY_LEFT:
                ship.move(Vector3f.UNIT_X.mult(SHIP_SPEED).negate()); // paddle two move up
                break;
            case KEY_RIGHT:
                ship.move(Vector3f.UNIT_X.mult(SHIP_SPEED)); // paddle two move down
                break;
            case KEY_SHOOT:
                if (refireTimer > REFIRE_TIME) {
                    makeMissile(ship.getLocalTranslation(), Vector3f.UNIT_Y, 2, ColorRGBA.White);
                    refireTimer = 0;
                }
                break;
        }
    }

    public boolean isClearPath(Spatial spatial) {
        Ray ray = new Ray(spatial.getLocalTranslation(), Vector3f.UNIT_Y.negate());
        CollisionResults collisionResults = new CollisionResults();
        rootNode.collideWith(ray, collisionResults);

        for (CollisionResult collisionResult : collisionResults) {
            if (collisionResult.getGeometry().getParent() != spatial) {
                if (collisionResult.getGeometry().getName().equals(ALIEN_NAME)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Hook into the render loop. Unused as the basic engine is adequate for the drawing we need in most cases.
     *
     * @param rm
     */
    @Override
    public void simpleRender(RenderManager rm) {
    }
}
