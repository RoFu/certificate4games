package certificate4.game.spaceinvaders;

import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import java.util.Random;

/**
 * The AlienControl is a class which is instantiated for each alien. It is used to control their behaviour.
 *
 * @author Roland Fuller
 */
public class AlienControl extends AbstractControl {

    private static final float MOVE_TIME = 1f; // how many seconds before I move again
    private Vector3f DOWN_JUMP = new Vector3f(0, -1, 0); // which direction to move in when I go down a row
    public static final int ALIEN_SPEED = 10; // how fast to move

    private SpaceInvaders application; // a reference back to the SpaceInvaders object to ask it to check if there are any friends below (for shooting)
    private RigidBodyControl rigidBodyControl; // the physics controller for the alien, to move it
    private Random random; // random generator is used for firing weapon
    private float moveTimer; // a variable to control the movent timing
    private Vector3f moveDirection = Vector3f.UNIT_X; // which direction to move in (side to side)

    /**
     * The constructor sets up the alien controller and passes in;
     *
     * @param application the space invaders object, so we can store a reference to it
     */
    public AlienControl(SpaceInvaders application) {
        this.application = application;
        this.random = new Random();
    }

    /**
     * Override the setSpatial method from the ABstractControl engine class. This method is automatically called by the
     * engine when we attach this controller to the container node/geometry to react to their connection.
     *
     * @param spatial
     */
    @Override
    public void setSpatial(Spatial spatial) {
        super.setSpatial(spatial);
        this.rigidBodyControl = spatial.getControl(RigidBodyControl.class);
        rigidBodyControl.setLinearVelocity(moveDirection.mult(ALIEN_SPEED));
    }

    /**
     * Override the update method from the AbstractControl to give our alien some AI
     *
     * @param tpf the time since the last frame
     */
    @Override
    protected void controlUpdate(float tpf) {
        // Very very occasionally, tell the spaceInvaders object that we want to spawn a missile
        if (random.nextFloat() > 0.999f) {
            if (application.isClearPath(spatial)) {
                application.makeMissile(spatial.getLocalTranslation(), Vector3f.UNIT_Y.negate(), 4, ColorRGBA.Green);
            }
        }

        // the timer goes up constantly, if we run out of time, we go down a row.
        moveTimer += tpf;
        if (moveTimer > MOVE_TIME) {
            moveTimer = 0;
            moveDirection = moveDirection.equals(Vector3f.UNIT_X) ? Vector3f.UNIT_X.negate() : Vector3f.UNIT_X;
            rigidBodyControl.setLinearVelocity(moveDirection.mult(ALIEN_SPEED));
            rigidBodyControl.setPhysicsLocation(rigidBodyControl.getPhysicsLocation().add(DOWN_JUMP));
        }
    }

    /**
     * We are forced to override this method even though we don't do anything with it.
     *
     * @param rm
     * @param vp
     */
    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }
}
