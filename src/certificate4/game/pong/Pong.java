package certificate4.game.pong;

import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.PhysicsTickListener;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Sphere;

/**
 * @author Roland Fuller The Pong class stands on the shoulders of the SimpleApplication, inheriting basic functionality
 * from the engine. This includes input, assets, access to the update and rendering loops, etc.
 *
 * It implements 3 interfaces, allowing the class to function in three additional ways:
 *
 * AnalogListener - Listen to key input messages so we can respond to them PhysicsCollisionListener - Listen to the
 * physics engine when it tells us a collision has occurred PhysicsTickListener - Hook into the physics engine updates
 * so we can modify the movement of the ball
 */
public class Pong extends SimpleApplication implements AnalogListener, PhysicsCollisionListener, PhysicsTickListener {

    // A bunch of constant variables so our code doesn't have magical numbers strewn about
    private static final Vector3f MASK_2D = new Vector3f(1, 1, 0); // This is used to make sure that the ball doesn't move on the Z axis by masking out it's z component
    private static final float PADDLE_SPEED = 0.02f; // How fast can the paddle move
    private static final float PADDLE_LENGTH = 1.5f; // How long is the paddle (other dimensions are hard-coded)
    private static final float PADDLE_MASS = 10f; // how heavy is the paddle (this effects collision maths with the ball)
    private static final float KNOCK_FACTOR = 4; // How much to bump the ball away at an angle from the paddle on collision
    private static final float BALL_MASS = 1f; // how heavy is the ball
    private static final float BALL_SPEED = 16; // How fast does the ball move
    private static final float BALL_SIZE = 0.25f; // How big is the ball (radius)

    // Names of the game objects for identification
    private static final String PADDLE_ONE_NAME = "PaddleOne";
    private static final String PADDLE_TWO_NAME = "PaddleTwo";
    private static final String BALL_NAME = "Ball";
    private static final String GOAL_ONE_NAME = "GoalOne";
    private static final String GOAL_TWO_NAME = "GoalTwo";
    private static final String WALL_NAME = "Wall";

    // Names of input commands
    private static final String ONE_UP = "OneUp";
    private static final String ONE_DOWN = "OneDown";
    private static final String TWO_UP = "TwoUp";
    private static final String TWO_DOWN = "TwoDown";
    private static final String NEW_BALL = "NewBall";

    // The main method which just creates a Pong object and hands over control to the engine
    public static void main(String[] args) {
        Pong app = new Pong();
        app.start();
    }

    // variables that we need to access throughout the program
    private BulletAppState bulletAppState; // a reference to the physics engine
    private Node paddleOne, paddleTwo; // the paddle objects - need to tell them to move
    private RigidBodyControl ball; // the physics controller for the ball
    private int twoScore = 0, onceScore = 0; // score of both players
    private BitmapText scoreOneText, scoreTwoText; // the text objects to display the scores

    /**
     * simpleInitApp - runs once only, when the game starts
     */
    @Override
    public void simpleInitApp() {
        setDisplayFps(false);
        setDisplayStatView(false);
        flyCam.setEnabled(false); // turn off the default WASD FPS camera
        cam.setLocation(new Vector3f(0, 0, 20)); // move the camera away from the scene a bit
        setDisplayStatView(false); // turn off the debugging statistics display
        initPhysics(); // set up the physics engine
        initKeys(); // set up the key input
        initScores(); // set up the score GUI objects
        initGameObjects(); // create all the game objects - ball, paddles, walls, goals
    }

    /**
     * Set up the physics engine
     */
    public void initPhysics() {
        stateManager.attach(bulletAppState = new BulletAppState()); // create and attach the physics engine - there isn't one by default
        bulletAppState.getPhysicsSpace().addCollisionListener(this); // tell the physics engine that this class (Pong) wants to know when a collision happens 
        bulletAppState.getPhysicsSpace().addTickListener(this); // tell the physics engine that this class (Pong) wants to know when the physics is being updated
        //bulletAppState.setDebugEnabled(true); // uncomment this line to show collision shape wireframes for debugging purposes
    }

    /**
     * Set up the user input keys
     */
    public void initKeys() {
        inputManager.addListener(this, new String[]{ONE_UP, ONE_DOWN, TWO_UP, TWO_DOWN, NEW_BALL}); // Tell the input manager this class (Pong) wants to hear about the following input commands
        inputManager.addMapping(ONE_UP, new KeyTrigger(KeyInput.KEY_W)); // attach this input command to this particular key
        inputManager.addMapping(ONE_DOWN, new KeyTrigger(KeyInput.KEY_S)); // attach this input command to this particular key
        inputManager.addMapping(TWO_UP, new KeyTrigger(KeyInput.KEY_UP)); // attach this input command to this particular key
        inputManager.addMapping(TWO_DOWN, new KeyTrigger(KeyInput.KEY_DOWN)); // attach this input command to this particular key
        inputManager.addMapping(NEW_BALL, new KeyTrigger(KeyInput.KEY_SPACE)); // attach this input command to this particular key
    }

    /**
     * Setup the objects used to display the player's scores
     */
    protected void initScores() {
        scoreOneText = new BitmapText(guiFont, false); // create a bitmap text object to be shown on the GUI
        scoreOneText.setSize(guiFont.getCharSet().getRenderedSize() * 4); // setup the size of the font
        // set the location to be one fifth the way across the screen and one sixth down (plus font size)
        scoreOneText.setLocalTranslation(settings.getWidth() / 5 - scoreOneText.getLineWidth() / 5, settings.getHeight() / 6 + scoreOneText.getLineHeight() / 6, 0);
        guiNode.attachChild(scoreOneText); // need to attach a bitmap object to the gui node so it can be updated/rendered by the engine

        // as above, for the second player's score - as this is a duplication it would be possible to generalise the method to do either and call it twice
        scoreTwoText = new BitmapText(guiFont, false);
        scoreTwoText.setSize(guiFont.getCharSet().getRenderedSize() * 4);
        scoreTwoText.setLocalTranslation((settings.getWidth() / 5) * 4 - scoreTwoText.getLineWidth() / 5, settings.getHeight() / 6 + scoreTwoText.getLineHeight() / 6, 0);
        guiNode.attachChild(scoreTwoText);
    }

    /**
     * Create all the game objects
     */
    public void initGameObjects() {
        makeWall(new Vector3f(0, -8, 0));
        makeWall(new Vector3f(0, 8, 0));
        makeGoal(GOAL_ONE_NAME, new Vector3f(-10, 0, 0));
        makeGoal(GOAL_TWO_NAME, new Vector3f(10, 0, 0));
        paddleOne = makePaddle(PADDLE_ONE_NAME, new Vector3f(-6, 0, 0), ColorRGBA.Red); // the paddle is created and passed back so we can store it in the variable to reference later...
        paddleTwo = makePaddle(PADDLE_TWO_NAME, new Vector3f(6, 0, 0), ColorRGBA.Blue);
        makeBall();
    }

    /**
     * Create a paddle object. This relies on the core makeGameObject method to set up the base object and then
     * customises it with the parameters and also extracts the physics controller to set it as a kinematic type of
     * physics object
     *
     * @param name - setup the name of the object - as either paddle needs a unique name
     * @param location - set the position of the object
     * @param colour - setup the colour of the object
     */
    public Node makePaddle(String name, Vector3f location, ColorRGBA colour) {
        Node paddleNode = makeGameObject(name, new Box(0.15f, PADDLE_LENGTH, 1), location, colour, PADDLE_MASS); // create the core Node object and return it
        RigidBodyControl rigidBodyControl = paddleNode.getControl(RigidBodyControl.class); // extract the physics controller from the game object
        rigidBodyControl.setKinematic(true); // ...so we can set it as a kinematic object
        return paddleNode; // return the game object back to the caller
    }

    /**
     * Make a ball object
     *
     * @return the completed ball object, not currently necessary but for consistency and so you could make more than
     * one ball...
     */
    public Node makeBall() {
        Node ballNode = makeGameObject(BALL_NAME, new Sphere(32, 32, BALL_SIZE), Vector3f.ZERO, ColorRGBA.White, BALL_MASS); // make the core object and return it back
        ball = ballNode.getControl(RigidBodyControl.class); // get access to the physics controller and store it in the ball variable for later use
        ball.setGravity(Vector3f.ZERO); // turn gravity off for the ball, otherwise it will fall down
        resetBall(ball, Vector3f.UNIT_X); // tell the ball to go to the middle and fly to the right
        return ballNode;
    }

    /**
     * Make a wall object by relying on the core makeGameObject method with custom parameters
     *
     * @param location - where the wall should be placed
     * @return the wall object node, not currently needed but for consistency
     */
    private Node makeWall(Vector3f location) {
        return makeGameObject(WALL_NAME, new Box(12, 1, 1), location, ColorRGBA.Yellow, 0);
    }

    /**
     * Make a goal wall
     *
     * @param name need this because either goal should be uniquely identified on collision
     * @param location
     * @return
     */
    public Node makeGoal(String name, Vector3f location) {
        return makeGameObject(name, new Box(1, 7, 1), location, ColorRGBA.Green, 0);
    }

    /**
     * This is the core method for creating any game object. It contains all the common ground that is required for
     * making any type of object, and takes in a bunch of parameters to help customise it to be different
     *
     * @param name setup the name of the object so we can identify it on collisions etc
     * @param mesh provide the model for the object (so this method is agnostic about how it looks)
     * @param location place to put the object
     * @param colour setup a colour for the material which will be applied to the model (we don't do texturing etc yet)
     * @param mass how heavy to make the physics controller of the object - effects its behaviour on collisions
     * @return
     */
    public Node makeGameObject(String name, Mesh mesh, Vector3f location, ColorRGBA colour, float mass) {
        Node node = new Node(name); // create a node object which is a container for all the components of the game object
        node.setLocalTranslation(location); // move the node objec to the desired location
        Geometry geometry = new Geometry(node.getName(), mesh); // create a geometry object that will be rendered with the model provided
        Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md"); // create a material (skin) for the model
        material.setColor("Color", colour); // set the colour of the material to the one provided
        geometry.setMaterial(material); // apply the material to the model
        RigidBodyControl rigidBodyControl = new RigidBodyControl(mass); // create a physics controller for the game object
        node.attachChild(geometry); // attach the model to the node container object
        node.addControl(rigidBodyControl); // attach the physics controller to the node container
        bulletAppState.getPhysicsSpace().add(rigidBodyControl); // add the physics controller to the physics engine so its movement can be managed by the engine
        rigidBodyControl.setFriction(0); // all objects in this game are very slipper
        rigidBodyControl.setRestitution(1); // all objects in this game are extremely bouncy
        rootNode.attachChild(node); // add the node object to the "scene graph" so they can be rendered and updated.
        return node; // return the completed object back to the caller so it can do further stuff with it
    }

    /**
     * This method is not used by is required by the PhysicsTickListener interface
     *
     * @param space
     * @param tpf
     */
    @Override
    public void prePhysicsTick(PhysicsSpace space, float tpf) {
    }

    /**
     * This is the method we're using from the PhysicsTickListener, it is called by the physics engine when the physics
     * is being updated. We need to hook this code in here because otherwise it will be out of synch with the physics
     * calculations and we will get choppy performance.
     *
     * @param space
     * @param tpf
     */
    @Override
    public void physicsTick(PhysicsSpace space, float tpf) {
        ball.setLinearVelocity(ball.getLinearVelocity().normalize().mult(BALL_SPEED));
        ball.setPhysicsLocation(ball.getPhysicsLocation().mult(MASK_2D));
    }

    /**
     * This is the method implemented by the PhysicsCollisionListener interface which is automatically called by the
     * physics engine any time two physics objects touch
     *
     * @param event
     */
    @Override
    public void collision(PhysicsCollisionEvent event) {
        // We use our resolve method to see if either node A or node B involved in the collision
        // is a goal, paddle, ball....
        Spatial goalOne = resolve(GOAL_ONE_NAME, event.getNodeA(), event.getNodeB());
        Spatial goalTwo = resolve(GOAL_TWO_NAME, event.getNodeA(), event.getNodeB());
        Spatial paddleOne = resolve(PADDLE_ONE_NAME, event.getNodeA(), event.getNodeB());
        Spatial paddleTwo = resolve(PADDLE_TWO_NAME, event.getNodeA(), event.getNodeB());
        Spatial ballSpatial = resolve(BALL_NAME, event.getNodeA(), event.getNodeB());

        // Once resolved, we can see which objects we are dealing with.
        // Everything is in a nested if, because we only care about collisions with the ball and (other thing)
        if (ballSpatial != null) {
            if (goalOne != null) { // player one's goal touched the ball
                twoScore++; // increment player two's score
                resetBall(ball, Vector3f.UNIT_X); // reset the location and direction of the ball
            } else if (goalTwo != null) {
                onceScore++;
                resetBall(ball, Vector3f.UNIT_X.negate());
            }
            if (paddleOne != null) { // when the paddles touch the ball, knock it a little bit sideways on top of the normal physics calculations
                knockBall(paddleOne.getControl(RigidBodyControl.class), ball);
            } else if (paddleTwo != null) {
                knockBall(paddleTwo.getControl(RigidBodyControl.class), ball);
            }
        }
    }

    /**
     * A simple utility method which checks to see which of two nodes has the name we're looking for
     *
     * @param name The name we're testing for
     * @param nodeA the first node involved in the collision
     * @param nodeB the second node involved in the collision
     * @return the node that had the name we were looking for
     */
    private Spatial resolve(String name, Spatial nodeA, Spatial nodeB) {
        if (nodeA.getName().equals(name)) {
            return nodeA;
        } else if (nodeB.getName().equals(name)) {
            return nodeB;
        }
        return null;
    }

    /**
     * This method looks at two physics controllers
     *
     * @param paddle the paddle involved in a collision
     * @param ball the ball involved in a collision The purpose is to knock the ball away at an angle rather than bounce
     * off the paddle flat
     */
    private void knockBall(RigidBodyControl paddle, RigidBodyControl ball) {
        // calculate the direction vector from the paddle to the ball. What this means is that we
        // figure out a line drawn from the center of the paddle to the center of the ball
        Vector3f direction = ball.getPhysicsLocation().subtract(paddle.getPhysicsLocation());
        // We set the movement of the ball to be whatever it currently is, but add on the direction we
        // worked out above and multiply by some factor to increase the effect
        ball.setLinearVelocity(ball.getLinearVelocity().add(direction.mult(KNOCK_FACTOR)));
    }

    /**
     * Set the location and the movement direction of the ball
     *
     * @param ball the ball to reset
     * @param direction the direction to move in
     */
    private void resetBall(RigidBodyControl ball, Vector3f direction) {
        ball.setPhysicsLocation(Vector3f.ZERO);
        ball.setLinearVelocity(direction.mult(BALL_SPEED));
    }

    /**
     * Hook into the update loop of the engine
     *
     * @param tpf
     */
    @Override
    public void simpleUpdate(float tpf) {
        // change the bitmapText objects to be whatever the score currently is.
        scoreOneText.setText(String.valueOf(onceScore));
        scoreTwoText.setText(String.valueOf(twoScore));
        // we do not do any physics calculations here because the update loop is out of synch with the physics loop (multi-threaded)
    }

    /**
     * The method implemented from the AnalogListener interface which allows us to respond to input manager when it
     * tells us that some key was pressed
     *
     * @param name the name of the command that occurred
     * @param value unused - the amount the key was pressed (only makes sense for thumbsticks etc, not keyboard)
     * @param tpf amount of time elapsed - unused
     */
    @Override
    public void onAnalog(String name, float value, float tpf) {
        // Switch on the name of the command that happened
        switch (name) {
            case ONE_UP:
                paddleOne.move(Vector3f.UNIT_Y.mult(PADDLE_SPEED)); // tell paddle one to move up at the given speed
                break;
            case ONE_DOWN:
                paddleOne.move(Vector3f.UNIT_Y.mult(PADDLE_SPEED).negate()); // paddle one move down
                break;
            case TWO_UP:
                paddleTwo.move(Vector3f.UNIT_Y.mult(PADDLE_SPEED)); // paddle two move up
                break;
            case TWO_DOWN:
                paddleTwo.move(Vector3f.UNIT_Y.mult(PADDLE_SPEED).negate()); // paddle two move down
                break;
            case NEW_BALL:
                makeBall();
                break;
        }
    }

    /**
     * Hook into the render loop. Unused as the basic engine is adequate for the drawing we need in most cases.
     *
     * @param rm
     */
    @Override
    public void simpleRender(RenderManager rm) {
    }
}
