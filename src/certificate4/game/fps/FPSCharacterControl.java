package certificate4.game.fps;

import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.math.Vector3f;

/**
 *
 * @author Roland Fuller
 */
public abstract class FPSCharacterControl extends BetterCharacterControl {

    // Names of input commands
    public static final float CHARACTER_SPEED = 1f; // How fast can the character move
    public static final float CHARACTER_MASS = 10f; // how heavy is the character
    public static final float CHARACTER_HEIGHT = 4f; // how heavy is the character
    public static final float CHARACTER_RADIUS = 2f; // how heavy is the character 
    public static final int CHARACTER_WALK_SPEED = 10;
    public static final float REFIRE_TIME = 0.5f; // how fast can the weapon can fire
    protected Vector3f EYE_OFFSET = new Vector3f(0, 1, 0);
    protected Vector3f MASK_2D = new Vector3f(1, 0, 1);
    protected float refireTimer = 0;
    private int health = 0;
    protected final FPS fps;

    /**
     * Pass back the values we've decided upon to the BetterCharacterControl class which can use them to set up the
     * correct scale of physics controller
     *
     * @param fps
     */
    public FPSCharacterControl(FPS fps) {
        super(CHARACTER_RADIUS, CHARACTER_HEIGHT, CHARACTER_MASS);
        this.fps = fps;
    }

    /**
     * @return the health
     */
    public int getHealth() {
        return health;
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        refireTimer += tpf; // our weapon is constantly cooling down
    }

    /**
     * A utility method for child classes They can reques that we fire our weapon
     *
     * @param from The character's position
     * @param to The direction to shoot in
     */
    protected void fire(Vector3f from, Vector3f to) {
        // Only fire if our cooldown time has expired
        if (refireTimer >= REFIRE_TIME) {
            refireTimer = 0;
            // Request the game fire a bullet for us
            fps.shoot(spatial, from, to);
        }
    }

}
