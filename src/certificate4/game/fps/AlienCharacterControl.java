package certificate4.game.fps;

import static certificate4.game.fps.FPS.PLAYER_NAME;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

/**
 *
 * @author Roland Fuller
 */
public class AlienCharacterControl extends FPSCharacterControl {

    // Keep track of whether we've ever seen the player in person
    private boolean seenPlayer = false;
    private Node player; // a link to the player game object so we can search for him

    /**
     * Alien Control is passed in the FPS object because the alien makes heavy use of the utility methods supplied by it
     *
     * @param fps
     */
    public AlienCharacterControl(FPS fps) {
        super(fps);
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);

        // Find out which direction the player is in
        Vector3f facePlayer = fps.getPlayerDirection(spatial);

        // Check to see if we have a direct line of sight to the player
        player = fps.lookFor(spatial, PLAYER_NAME, spatial.getLocalTranslation(), facePlayer);

        // Walk towards the player, firing, if we know where they are
        if (seenPlayer) {
            setWalkDirection(facePlayer.mult(MASK_2D).mult(CHARACTER_WALK_SPEED));
            fire(spatial.getLocalTranslation().add(EYE_OFFSET), facePlayer);
        } else {
            // When we see the player
            if (player != null) {
                // Our whole world changes - now we can't unsee the player ever again
                seenPlayer = true;
            }
        }
    }
}
