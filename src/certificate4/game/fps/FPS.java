package certificate4.game.fps;

// We need a bunch of classes from the libraries
import static certificate4.game.fps.PlayerCharacterControl.JUMP;
import static certificate4.game.fps.PlayerCharacterControl.SHOOT;
import static certificate4.game.fps.PlayerCharacterControl.STRAFE_LEFT;
import static certificate4.game.fps.PlayerCharacterControl.STRAFE_RIGHT;
import static certificate4.game.fps.PlayerCharacterControl.WALK_BACKWARD;
import static certificate4.game.fps.PlayerCharacterControl.WALK_FORWARD;
import com.jme3.app.SimpleApplication;
import com.jme3.audio.AudioData;
import com.jme3.audio.AudioNode;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Sphere;

/**
 * A basic FPS game with a maze, enemies with simple AI, a ray casting weapon and an exit
 *
 * @author Roland Fuller
 */
public class FPS extends SimpleApplication implements PhysicsCollisionListener {

    public static final int MAP_SCALE = 10;

    // Names of the game objects for identification
    public static final String PLAYER_NAME = "Player";
    private static final String ALIEN_NAME = "Alien";
    private static final String WALL_NAME = "Wall";
    private static final String FLOOR_NAME = "Floor";
    private static final String EXIT_NAME = "Exit";

    // Values used to compose a simple map - these are the things we can spawn in the world
    public static final int WALL = 1;
    public static final int SPAWN = 2;
    public static final int ENEMY = 4;
    public static final int EXIT = 3;
    public static final int BLANK = 0;

    // Here's the map which is a 2d array that shows where each entity will be placed
    private static final int[][] MAP = {{WALL, WALL, WALL, WALL, WALL, WALL, WALL, WALL, WALL, WALL},
    {WALL, SPAWN, BLANK, BLANK, BLANK, BLANK, WALL, ENEMY, BLANK, WALL},
    {WALL, WALL, WALL, BLANK, WALL, ENEMY, WALL, ENEMY, BLANK, WALL},
    {WALL, ENEMY, WALL, BLANK, ENEMY, WALL, WALL, WALL, BLANK, WALL},
    {WALL, BLANK, WALL, BLANK, WALL, BLANK, BLANK, BLANK, ENEMY, WALL},
    {WALL, BLANK, BLANK, ENEMY, WALL, ENEMY, WALL, WALL, BLANK, WALL},
    {WALL, BLANK, WALL, WALL, WALL, BLANK, WALL, ENEMY, BLANK, WALL},
    {WALL, BLANK, ENEMY, WALL, ENEMY, BLANK, BLANK, ENEMY, BLANK, WALL},
    {WALL, BLANK, ENEMY, BLANK, BLANK, BLANK, ENEMY, WALL, ENEMY, EXIT},
    {WALL, WALL, WALL, WALL, WALL, WALL, WALL, WALL, WALL, WALL}};

    // The main method which just creates a SpaceInvaders object and hands over control to the engine
    public static void main(String[] args) {
        FPS app = new FPS();
        app.start();
    }

    // variables that we need to access throughout the program
    private BulletAppState bulletAppState; // a reference to the physics engine
    private Node player; // the player game object - need to store a direct link for convenience
    private BitmapText healthText, crossHair; // the text objects to display the scores
    private PlayerCharacterControl playerControl; // hang on to the player controller so we can talk to it in the update
    private AudioNode gunFire, ricochet, alien; // create nodes to store each sound effect

    /**
     * simpleInitApp - runs once only, when the game starts
     */
    @Override
    public void simpleInitApp() {
        setDisplayFps(false);
        setDisplayStatView(false);
        flyCam.setMoveSpeed(0);
        setDisplayStatView(false); // turn off the debugging statistics display
        initPhysics(); // set up the physics engine
        initGUI(); // set up the score GUI objects
        initSounds(); // load up sound effects
        initKeys(); // setup input
        startGame(); // begin the game
    }

    /**
     * Reset the game state by deleting all game objects and reload the map from scratch You could add a parameter
     * specifying which level you're up to to load a different map data
     */
    private void startGame() {
        if (player != null) {
            inputManager.removeListener(playerControl); // detatch the mapping for input from the player as we'll create a new player now
        }
        bulletAppState.getPhysicsSpace().removeAll(rootNode); // remove everything from the physics engine
        rootNode.detachAllChildren(); // prune the rootNote tree from the trunk so all objects are gone
        makeMap(); // create all the game objects - must happen last because the above may be dependencies
    }

    /**
     * Load up all the sound effects and store references to them so we can play them directly without having to look
     * them up in the scene graph first
     */
    public void initSounds() {
        gunFire = new AudioNode(assetManager, "Sounds/9mm.wav", AudioData.DataType.Buffer);
        ricochet = new AudioNode(assetManager, "Sounds/ricochet.wav", AudioData.DataType.Buffer);
        alien = new AudioNode(assetManager, "Sounds/alien.wav", AudioData.DataType.Buffer);
        // Turn off positionality for sounds to simplify code for playing them
        gunFire.setPositional(false);
        ricochet.setPositional(false);
        alien.setPositional(false);
    }

    /**
     * Set up the physics engine
     */
    public void initPhysics() {
        stateManager.attach(bulletAppState = new BulletAppState()); // create and attach the physics engine - there isn't one by default
        bulletAppState.getPhysicsSpace().addCollisionListener(this); // tell the physics engine that this class (Pong) wants to know when a collision happens 
        //bulletAppState.setDebugEnabled(true);
    }

    /**
     * Set up the user input keys
     */
    public void initKeys() {
        inputManager.addMapping(SHOOT, new MouseButtonTrigger(MouseInput.BUTTON_LEFT)); // attach this input command to this particular key
        inputManager.addMapping(STRAFE_LEFT, new KeyTrigger(KeyInput.KEY_A)); // attach this input command to this particular key
        inputManager.addMapping(STRAFE_RIGHT, new KeyTrigger(KeyInput.KEY_D)); // attach this input command to this particular key
        inputManager.addMapping(WALK_FORWARD, new KeyTrigger(KeyInput.KEY_W)); // attach this input command to this particular key
        inputManager.addMapping(WALK_BACKWARD, new KeyTrigger(KeyInput.KEY_S)); // attach this input command to this particular key
        inputManager.addMapping(JUMP, new KeyTrigger(KeyInput.KEY_SPACE)); // attach this input command to this particular key
    }

    /**
     * Setup the objects used to display the player's scores
     */
    protected void initGUI() {
        healthText = new BitmapText(guiFont, false); // create a bitmap text object to be shown on the GUI
        healthText.setSize(guiFont.getCharSet().getRenderedSize() * 4); // setup the size of the font
        // set the location to be one fifth the way across the screen and one sixth down (plus font size)
        healthText.setLocalTranslation(0, settings.getHeight() + healthText.getLineHeight() / 6, 0);
        guiNode.attachChild(healthText); // need to attach a bitmap object to the gui node so it can be updated/rendered by the engine

        crossHair = new BitmapText(guiFont, false); // create a bitmap text object to be shown on the GUI   
        crossHair.setSize(guiFont.getCharSet().getRenderedSize() * 2); // setup the size of the font 
        crossHair.setText("+");
        // set the location to be one fifth the way across the screen and one sixth down (plus font size)
        crossHair.setLocalTranslation( // center
                settings.getWidth() / 2 - guiFont.getCharSet().getRenderedSize() / 2,
                settings.getHeight() / 2 + crossHair.getLineHeight() / 2, 0);
        guiNode.attachChild(crossHair); // need to attach a bitmap object to the gui node so it can be updated/rendered by the engine
    }

    /**
     * Loop through each of the locations in the map data and examine what it says to place there. Everything should be
     * according to the map scale so this is adjustable.
     */
    public void makeMap() {
        makeFloor();
        for (int i = 0; i < MAP.length; i++) {
            for (int j = 0; j < MAP.length; j++) {
                switch (MAP[i][j]) {
                    case BLANK:
                        break;
                    case WALL:
                        makeWall(new Vector3f(i * MAP_SCALE, 1, j * MAP_SCALE));
                        break;
                    case SPAWN:
                        player = makePlayer(new Vector3f(i * MAP_SCALE, 1, j * MAP_SCALE));
                        break;
                    case EXIT:
                        makeExit(new Vector3f(i * MAP_SCALE, 1, j * MAP_SCALE));
                        break;
                    case ENEMY:
                        makeEnemy(new Vector3f(i * MAP_SCALE, 1, j * MAP_SCALE));
                        break;
                }
            }
        }
    }

    /**
     * Make an alien monster from beyond mars
     *
     * @param position
     */
    public void makeEnemy(Vector3f position) {
        makeCharacter(ALIEN_NAME, new Sphere(32, 32, 1), position, ColorRGBA.Red, new AlienCharacterControl(this));
    }

    /**
     * Make a human controlled player which relies on the core character with some embellishments i.e use specifically a
     * player control
     *
     * @param location
     * @return
     */
    public Node makePlayer(Vector3f location) {
        player = makeCharacter(PLAYER_NAME, new Sphere(32, 32, 1), location, ColorRGBA.Blue, new PlayerCharacterControl(this, cam));
        playerControl = player.getControl(PlayerCharacterControl.class); // extract the physics controller from the game object
        inputManager.addListener(playerControl, new String[]{SHOOT, STRAFE_LEFT, STRAFE_RIGHT, WALK_FORWARD, WALK_BACKWARD, JUMP}); // Tell the input manager this class (Pong) wants to hear about the following input commands
        return player; // return the game object back to the caller
    }

    /**
     * Make a wall object by relying on the core makeGameObject method with custom parameters
     *
     * @param location - where the wall should be placed
     * @return the wall object node, not currently needed but for consistency
     */
    private void makeWall(Vector3f location) {
        // create a block of cubes with physics controllers all clumped together
        makeGameObject(WALL_NAME, new Box(MAP_SCALE / 2, MAP_SCALE / 2, MAP_SCALE / 2), location, ColorRGBA.randomColor().mult(0.1f), 0);
    }

    /**
     * Make a floor the same size as the map array
     */
    private void makeFloor() {
        // create a block of cubes with physics controllers all clumped together
        makeGameObject(FLOOR_NAME, new Box(MAP_SCALE * MAP.length, 1f, MAP_SCALE * MAP.length), new Vector3f(0, 0, 0), ColorRGBA.DarkGray, 0);
    }

    /**
     * Create the exit portal which will win the game to walk through
     *
     * @param vector3f
     */
    private void makeExit(Vector3f location) {
        // create a block of cubes with physics controllers all clumped together
        makeGameObject(EXIT_NAME, new Box(1, 10f, 1), location, ColorRGBA.Green, 0);
    }

    /**
     * This is the core method for creating any game object. It contains all the common ground that is required for
     * making any type of object, and takes in a bunch of parameters to help customise it to be different
     *
     * @param name setup the name of the object so we can identify it on collisions etc
     * @param mesh provide the model for the object (so this method is agnostic about how it looks)
     * @param location place to put the object
     * @param colour setup a colour for the material which will be applied to the model (we don't do texturing etc yet)
     * @param mass how heavy to make the physics controller of the object - effects its behaviour on collisions
     * @return
     */
    public Node makeGameObject(String name, Mesh mesh, Vector3f location, ColorRGBA colour, float mass) {
        Node node = new Node(name); // create a node object which is a container for all the components of the game object
        node.setLocalTranslation(location); // move the node objec to the desired location
        Geometry geometry = new Geometry(node.getName(), mesh); // create a geometry object that will be rendered with the model provided
        Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md"); // create a material (skin) for the model
        material.setColor("Color", colour); // set the colour of the material to the one provided
        geometry.setMaterial(material); // apply the material to the model
        RigidBodyControl rigidBodyControl = new RigidBodyControl(mass); // create a physics controller for the game object
        node.attachChild(geometry); // attach the model to the node container object
        node.addControl(rigidBodyControl); // attach the physics controller to the node container
        bulletAppState.getPhysicsSpace().add(rigidBodyControl); // add the physics controller to the physics engine so its movement can be managed by the engine
        rootNode.attachChild(node); // add the node object to the "scene graph" so they can be rendered and updated.
        return node; // return the completed object back to the caller so it can do further stuff with it
    }

    /**
     * Make specifically a character. This could be a player or an enemy as they share a lot of code We don't currently
     * (but could possibly) reuse the code from makeGameObject so there's less repetition
     *
     * @param name String name identifier
     * @param mesh the model
     * @param location where to place
     * @param colour colour of the material
     * @param fpsCharacterControl we supply this to make the character unique - controlled by the player or by an AI.
     * @return
     */
    public Node makeCharacter(String name, Mesh mesh, Vector3f location, ColorRGBA colour, FPSCharacterControl fpsCharacterControl) {
        Node node = new Node(name); // create a node object which is a container for all the components of the game object
        node.setLocalTranslation(location); // move the node objec to the desired location
        Geometry geometry = new Geometry(node.getName(), mesh); // create a geometry object that will be rendered with the model provided
        Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md"); // create a material (skin) for the model
        material.setColor("Color", colour); // set the colour of the material to the one provided
        geometry.setMaterial(material); // apply the material to the model
        node.attachChild(geometry); // attach the model to the node container object
        node.addControl(fpsCharacterControl); // attach the physics controller to the node container
        bulletAppState.getPhysicsSpace().add(fpsCharacterControl); // add the physics controller to the physics engine so its movement can be managed by the engine
        rootNode.attachChild(node); // add the node object to the "scene graph" so they can be rendered and updated.
        return node; // return the completed object back to the caller so it can do further stuff with it
    }

    /**
     * This is the method implemented by the PhysicsCollisionListener interface which is automatically called by the
     * physics engine any time two physics objects touch
     *
     * @param event
     */
    @Override
    public void collision(PhysicsCollisionEvent event) {
        // We use our resolve method to see if either node A or node B involved in the collision was a known object
        Spatial exit = resolve(EXIT_NAME, event.getNodeA(), event.getNodeB());
        Spatial player = resolve(PLAYER_NAME, event.getNodeA(), event.getNodeB());

        // Once resolved, we can see which objects we are dealing with.
        // Right now we only care about if the player hit the exit point
        if ((exit != null) && (player != null)) {
            startGame();
        }
    }

    /**
     * Used to totally remove a game object from the world. The object dissapears immediately, so controlling some kind
     * of fade effect might be wanted.
     *
     * @param gameObject node which is the root of the game object to delete
     */
    private void killAlien(Spatial gameObject) {
        alien.playInstance();
        bulletAppState.getPhysicsSpace().removeAll(gameObject);
        gameObject.removeFromParent();
    }

    /**
     * A simple utility method which checks to see which of two nodes has the name we're looking for
     *
     * @param name The name we're testing for
     * @param nodeA the first node involved in the collision
     * @param nodeB the second node involved in the collision
     * @return the node that had the name we were looking for
     */
    private Spatial resolve(String name, Spatial nodeA, Spatial nodeB) {
        if (nodeA.getName().equals(name)) {
            return nodeA;
        } else if (nodeB.getName().equals(name)) {
            return nodeB;
        }
        return null;
    }

    /**
     * Utility method provided by the game logic to any requesting friendly aliens which will inform them of where the
     * player is by using some vector mathemagics The trick is if you subtract one positional vector from another,
     * curiously the vector result you get is a directional vector which points from one position to the other!
     *
     * @param spatial the game object we want to find the direction from
     * @return the direction to the player (player should be parameterised so we can get the direction to *any* game
     * object from another but this is not currently required)
     */
    public Vector3f getPlayerDirection(Spatial spatial) {
        return player.getLocalTranslation().subtract(spatial.getLocalTranslation()).normalize();
    }

    /**
     * Check using the ray casting method, whether we are in the line of sight of a specified target
     *
     * @param spatial who is asking to cast a ray from (so we can cull them from the collision results)
     * @param target the name of the object we are looking for
     * @param from the position we are casting from
     * @param to the direction we're looking in
     * @return the game object, if found, otherwise null
     */
    public Node lookFor(Spatial spatial, String target, Vector3f from, Vector3f to) {
        Node found = castRay(spatial, from, to); // utilise the ray casting method to do the line drawing
        if (found != null) {
            if (found.getName().equals(target)) {
                return found;
            }
        }
        return null;
    }

    /**
     * Use ray casting to draw a line in the world from a starting game object in a given direction
     *
     * @param spatial the game object we're firing the ray from, use its position
     * @param to the *direction* we are firing in. (NOT some kind of position to fire to...)
     */
    public void shoot(Spatial spatial, Vector3f from, Vector3f to) {
        gunFire.playInstance(); // play the gun sound
        final Node node = castRay(spatial, from, to);
        if (node != null) {
            hit(node);
        }
    }

    /**
     * Use ray casting to draw a line in the world from a starting game object in a given direction
     *
     * @param spatial the game object we're firing the ray from, use its position
     * @param to the direction we are firing in.
     */
    private Node castRay(Spatial spatial, Vector3f from, Vector3f to) {
        Ray ray = new Ray(from, to); // create a line
        CollisionResults collisionResults = new CollisionResults(); // Create a storage object to hold collision results found
        rootNode.collideWith(ray, collisionResults); // check if the line intersects any models in the scene graph, from the rootNode down

        // Loop through all the collision results
        for (CollisionResult collisionResult : collisionResults) {
            // check if the collision's model has a parent node which is not the firer of the ray (don't hit ourselves)
            if (collisionResult.getGeometry().getParent() != spatial) {
                return collisionResult.getGeometry().getParent();
            }
        }
        return null;
    }

    /**
     * When a game object gets hit, we react to it here
     *
     * @param node
     */
    private void hit(Node node) {
        switch (node.getName()) {
            case ALIEN_NAME:
                killAlien(node);
                break;
            case WALL_NAME:
                ricochet.playInstance();
                break;
        }
    }

    /**
     * Hook into the update loop of the engine
     *
     * @param tpf
     */
    @Override
    public void simpleUpdate(float tpf) {
        // change the bitmapText objects to be whatever the player's health currently is.
        healthText.setText("health: " + playerControl.getHealth());
    }

    /**
     * Hook into the render loop. Unused as the basic engine is adequate for the drawing we need in most cases.
     *
     * @param rm
     */
    @Override
    public void simpleRender(RenderManager rm) {
    }

}
