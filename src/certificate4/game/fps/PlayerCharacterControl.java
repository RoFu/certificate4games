package certificate4.game.fps;

import com.jme3.input.controls.ActionListener;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;

/**
 *
 * @author Roland Fuller
 */
public class PlayerCharacterControl extends FPSCharacterControl implements ActionListener {

    // Some constants which define the controls the player can use
    public static final String JUMP = "Jump";
    public static final String SHOOT = "Shoot";
    public static final String STRAFE_LEFT = "Left";
    public static final String STRAFE_RIGHT = "Right";
    public static final String WALK_FORWARD = "Forward";
    public static final String WALK_BACKWARD = "Backward";

    private Camera camera; // a reference to the engine's camera so we can make it follow us
    private boolean forward, backward, left, right, fire; // whether we've pressed a key or not

    /**
     * Give the player controller access to the main game logic object so it can make use of the utility methods found
     * there
     *
     * @param fps - give me the game object
     * @param camera the camera to control
     */
    public PlayerCharacterControl(FPS fps, Camera camera) {
        super(fps);
        this.camera = camera;
    }

    /**
     * The input listener (which is attached in the makePlayer method in the FPS class) which will be notified when any
     * of the mapped keys are pressed, giving our player controller the ability to respond to input
     *
     * @param name the name of the mapped control
     * @param isPressed whether this key has been pressed or released
     * @param tpf the time elapsed
     */
    @Override
    public void onAction(String name, boolean isPressed, float tpf) {
        switch (name) {
            case WALK_FORWARD:
                forward = isPressed; // just set a boolean here so our update can process it
                break;
            case WALK_BACKWARD:
                backward = isPressed;
                break;
            case STRAFE_LEFT:
                left = isPressed;
                break;
            case STRAFE_RIGHT:
                right = isPressed;
                break;
            case SHOOT:
                fire = isPressed;
                break;
            case JUMP:
                jump(); // calls on the underlying BetterCharacterControl implementation
                break;
        }
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        Vector3f walkDirection = new Vector3f(); // make a new zero vector
        Vector3f moveDir = camera.getDirection(); // get the camera's direction
        camera.setLocation(spatial.getLocalTranslation().add(EYE_OFFSET)); // move camera to player's location, up a bit off the floor
        setViewDirection(camera.getDirection()); // tell the BetterCharacterControl to face the same way the camera is pointing
        // When the player presses the mapped keys, we should respond by building a vector
        // which will point in a reasonable way according to those keys. We add to the blank
        // walkDirection vector until it points in a combination of all the directions we pressed
        if (forward) {
            walkDirection.addLocal(moveDir);
        }
        if (backward) {
            walkDirection.addLocal(moveDir.negate());
        }
        if (left) {
            // when we cross a vector with another we get a direction which is right angles to both (to the side)
            walkDirection.addLocal(moveDir.cross(Vector3f.UNIT_Y).negate());
        }
        if (right) {
            walkDirection.addLocal(moveDir.cross(Vector3f.UNIT_Y));
        }
        if (fire) {
            // Utilise the underlying FPSCharacterControl logic to fire the weapon
            fire(camera.getLocation(), camera.getDirection());
        }
        // Tell the BetterCharacterControl to walk in the direction we calculated
        // but make sure it's clamped parrallel to the ground (so we can't walk up into the air)
        setWalkDirection(walkDirection.normalize().mult(MASK_2D).mult(CHARACTER_WALK_SPEED));
    }

}
