package certificate4.game.numberguessing;

import com.jme3.app.SimpleApplication;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import java.util.Random;

/**
 * @author Roland Fuller
 */
public class NumberGuessingGame extends SimpleApplication implements ActionListener {

    private static final int NUM_LINES = 3; // the number of text lines we want 
    private static final int WELCOME_LINE = 0; // the line to place the welcome message on
    private static final int USER_INPUT_LINE = 1; // the line to display user input on
    private static final int RESULT_LINE = 2; // the line to display the result on
    private BitmapText[] textLines; // An array of BitmapText objects. Each one holds a text string to display
    private String input = ""; // the player's input needs to be initialised to a blank string otherwise it's null
    private int target; // the number to guess

    /**
     * Entry point of the program
     *
     * @param args
     */
    public static void main(String[] args) {
        NumberGuessingGame app = new NumberGuessingGame();// creates an object of this class's type and...
        app.start(); // passes over control to the inherited engine method, which fires off the game loop
    }

    @Override
    public void simpleInitApp() {
        setDisplayFps(false);
        setDisplayStatView(false);
        setupInput();
        setupRandomNumber();
        setupText();
    }

    /**
     * Setup the key mapping
     */
    public void setupInput() {
        inputManager.addListener(this, "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "Enter"); // set up the valid actions
        inputManager.addMapping("0", new KeyTrigger(KeyInput.KEY_0)); // pair actions to the correct keys
        inputManager.addMapping("1", new KeyTrigger(KeyInput.KEY_1));
        inputManager.addMapping("2", new KeyTrigger(KeyInput.KEY_2));
        inputManager.addMapping("3", new KeyTrigger(KeyInput.KEY_3));
        inputManager.addMapping("4", new KeyTrigger(KeyInput.KEY_4));
        inputManager.addMapping("5", new KeyTrigger(KeyInput.KEY_5));
        inputManager.addMapping("6", new KeyTrigger(KeyInput.KEY_6));
        inputManager.addMapping("7", new KeyTrigger(KeyInput.KEY_7));
        inputManager.addMapping("8", new KeyTrigger(KeyInput.KEY_8));
        inputManager.addMapping("9", new KeyTrigger(KeyInput.KEY_9));
        inputManager.addMapping("Enter", new KeyTrigger(KeyInput.KEY_RETURN));
    }

    /**
     * Generate a random number
     */
    public void setupRandomNumber() {
        Random random = new Random(); // Creates a random number generator
        target = random.nextInt(100); //grab a random number in the range 0-99
        System.out.println(target); // for debugging - print the number on the console
    }

    /**
     * Setup our lines of text
     */
    public void setupText() {
        textLines = new BitmapText[NUM_LINES]; // initialise the array to hold this many objects

        // a number of times we repeat...
        for (int i = 0; i < NUM_LINES; i++) {
            textLines[i] = makeTextLine(i); // creating a text object and adding it to the array
        }

        // Now we use our utility method to set the text of a given line
        setLineText(WELCOME_LINE, "Welcome to my number guessing game...... please enter your guess between 0-99 followed by Enter...");
    }

    /**
     * Create a bitmap text object at a given location
     *
     * @param num the line number so we can place it on the screen
     * @return the created bitmap text object
     */
    public BitmapText makeTextLine(int num) {
        BitmapText line = new BitmapText(guiFont, false); // create the bitmaptext object with a default font in left to right reading order
        line.setSize(guiFont.getCharSet().getRenderedSize()); // set the size of the text for this bitmaptext to the native size of the font
        line.setLocalTranslation(0, settings.getHeight() - (num * line.getSize()), 0); // position the text on the screen according to the resolution and line number
        guiNode.attachChild(line); // we need to attach the bitmaptext to the guiNode so it will be rendered
        return line; // return to caller with the finished bitmaptext
    }

    /**
     * A utility method to safely set the text of a given bitmaptext by line number
     *
     * @param lineNumber the line number to be setting
     * @param text the text to place at this position
     */
    private void setLineText(int lineNumber, String text) {
        // the try block attempts to do something which may be unsafe
        try {
            textLines[lineNumber].setText(text); // note the given index may not exist
        } catch (ArrayIndexOutOfBoundsException aiobe) { // what to do if this particular exception occurred
            System.out.println("Can't set text line " + lineNumber + " because there are only " + textLines.length + " text lines!");
        }
    }

    /**
     * Called when an input to which this listener is registered to is invoked. In this case, we just accumulate the
     * input in the input string as we're building up a number
     *
     * @param name The name of the mapping that was invoked
     * @param isPressed True if the action is "pressed", false otherwise
     * @param tpf The time per frame value. (how long it's been since the last time we were called)
     */
    @Override
    public void onAction(String name, boolean isPressed, float tpf) {
        if (isPressed) {
            if (name.equals("Enter")) { // submit number
                try {
                    int guessAsInt = Integer.parseInt(input); // try to convert the input to an integer
                    checkGuess(guessAsInt); // check if it was correct
                } catch (NumberFormatException nfe) {
                    setLineText(RESULT_LINE, "That's not a number. Try again."); // in the case conversion failed
                }
                input = ""; //reset our guess
            } else {// accumulate number
                input += name;
                setLineText(USER_INPUT_LINE, input);
            }
        }
    }

    /**
     * Test your guess against the actual number. Display a suitable message.
     *
     * @param guess your guess
     */
    private void checkGuess(int guess) {
        if (guess == target) { // check if the guess is the same as the random number
            setLineText(RESULT_LINE, "You got it!!!");
        } else if (guess < target) { // otherwise, check if we are too low
            setLineText(RESULT_LINE, "Bit low that time...");
        } else { // otherwise, logically, they must have guessed too high
            setLineText(RESULT_LINE, "Bit high that time....");
        }
    }
}
