package certificate4.game.scissorspaperrock; //this class belong in this package

import static certificate4.game.scissorspaperrock.ScissorsPaperRock.Move.PAPER;
import static certificate4.game.scissorspaperrock.ScissorsPaperRock.Move.ROCK;
import static certificate4.game.scissorspaperrock.ScissorsPaperRock.Move.SCISSORS;
import com.jme3.app.SimpleApplication;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Sphere;

/**
 * Here's the class which exists in a file of the same name
 *
 * @author Roland Fuller
 */
public class ScissorsPaperRock extends SimpleApplication implements ActionListener {

    // define all the possible moves in the game
    public enum Move {
        SCISSORS, PAPER, ROCK;

        static Move next(Move move) {
            return move.equals(Move.ROCK) ? Move.SCISSORS : move.equals(Move.SCISSORS) ? Move.PAPER : Move.ROCK;
        }
    };
    public static final String MOVE = "Move";
    private static final int NUM_LINES = 25; // the number of text lines we want 
    private BitmapText[] textLines; // An array of BitmapText objects. Each one holds a text string to display
    private Node player1;
    private Node player2;
    private int player1Score;
    private int player2Score;
    private float roundTimer;
    private static final float ROUND_TIME = 1;

    /**
     * The entry point of the game
     *
     * @param args
     */
    public static void main(String[] args) {
        ScissorsPaperRock scissorsPaperRock = new ScissorsPaperRock();
        scissorsPaperRock.start();
    }

    @Override
    public void simpleInitApp() {
        flyCam.setEnabled(false);
        cam.setLocation(new Vector3f(0, 0, 20));
        setupInput();
        player1 = makePlayer(new Vector3f(-4, 0, 0), Vector3f.UNIT_X);
        player2 = makePlayer(new Vector3f(4, 0, 0), Vector3f.UNIT_X.negate());
    }

    public Node makePlayer(Vector3f location, Vector3f direction) {
        Node node = new Node();
        node.setUserData(MOVE, Move.ROCK);
        node.setLocalTranslation(location);

        node.attachChild(makeBox(direction.mult(3), ColorRGBA.Red));
        direction = rotateVector(direction);
        node.attachChild(makeBox(direction.mult(3), ColorRGBA.Green));
        direction = rotateVector(direction);
        node.attachChild(makeBox(direction.mult(3), ColorRGBA.Blue));

        rootNode.attachChild(node);
        return node;
    }

    public Vector3f rotateVector(Vector3f vector) {
        Quaternion quaternion = new Quaternion();
        quaternion.fromAngles(0, 0, FastMath.DEG_TO_RAD * (360 / 3));
        return quaternion.mult(vector);
    }

    public void rotatePlayer(Node node, boolean reverse) {
        Quaternion quaternion = new Quaternion();
        float amount = reverse ? FastMath.DEG_TO_RAD * (360 / 3) : -FastMath.DEG_TO_RAD * (360 / 3);
        quaternion.fromAngles(0, 0, amount);
        node.rotate(quaternion);

        // rotate enum as well (get node's move data, rotates to the next move and then saves the new value)
        node.setUserData(MOVE, Move.next((Move)node.getUserData(MOVE)));
    }

    public Spatial makeBox(Vector3f location, ColorRGBA colour) {
        Sphere b = new Sphere(64, 64, 1);
        Geometry geom = new Geometry("Box", b);
        geom.setLocalTranslation(location);

        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", colour);
        geom.setMaterial(mat);

        return geom;
    }

    /**
     * Setup the key mapping
     */
    public void setupInput() {
        inputManager.addListener(this, "P1UP", "P1DOWN", "P2UP", "P2DOWN"); // set up the valid actions
        inputManager.addMapping("P1UP", new KeyTrigger(KeyInput.KEY_A)); // pair actions to the correct keys
        inputManager.addMapping("P1DOWN", new KeyTrigger(KeyInput.KEY_Z));
        inputManager.addMapping("P2UP", new KeyTrigger(KeyInput.KEY_UP));
        inputManager.addMapping("P2DOWN", new KeyTrigger(KeyInput.KEY_DOWN));
    }

    /**
     * Called when an input to which this listener is registered to is invoked. In this case, we just accumulate the
     * input in the input string as we're building up a number
     *
     * @param name The name of the mapping that was invoked
     * @param isPressed True if the action is "pressed", false otherwise
     * @param tpf The time per frame value. (how long it's been since the last time we were called)
     */
    @Override
    public void onAction(String name, boolean isPressed, float tpf) {
        if (isPressed) {
            switch (name) {
                case "P1UP":
                    rotatePlayer(player1, true);
                    break;
                case "P1DOWN":
                    rotatePlayer(player1, false);
                    break;
                case "P2UP":
                    rotatePlayer(player2, true);
                    break;
                case "P2DOWN":
                    rotatePlayer(player2, false);
                    break;
            }
        }
    }

    @Override
    public void simpleUpdate(float tpf) {
        super.simpleUpdate(tpf);
        roundTimer += tpf;
        if (roundTimer >= ROUND_TIME) {
            roundTimer = 0;
            //check();
        }
        System.out.println("P1: " + player1Score);

        System.out.println("P1: " + player2Score);
    }

    /**
     * Create a bitmap text object at a given location
     *
     * @param num the line number so we can place it on the screen
     * @return the created bitmap text object
     */
    public BitmapText makeTextLine(int num) {
        BitmapText line = new BitmapText(guiFont, false); // create the bitmaptext object with a default font in left to right reading order
        line.setSize(guiFont.getCharSet().getRenderedSize()); // set the size of the text for this bitmaptext to the native size of the font
        line.setLocalTranslation(0, settings.getHeight() - (num * line.getSize()), 0); // position the text on the screen according to the resolution and line number
        guiNode.attachChild(line); // we need to attach the bitmaptext to the guiNode so it will be rendered
        return line; // return to caller with the finished bitmaptext
    }

    /**
     * A utility method to safely set the text of a given bitmaptext by line number
     *
     * @param lineNumber the line number to be setting
     * @param text the text to place at this position
     */
    private void setLineText(int lineNumber, String text) {
        // the try block attempts to do something which may be unsafe
        try {
            textLines[lineNumber].setText(text); // note the given index may not exist
        } catch (ArrayIndexOutOfBoundsException aiobe) { // what to do if this particular exception occurred
            System.out.println("Can't set text line " + lineNumber + " because there are only " + textLines.length + " text lines!");
        }
    }

    /**
     * Game logic to determine which move wins.
     */
    private void check(Move a, Move b) {
        switch (a) {
            case SCISSORS:
                switch (b) {
                    case PAPER:
                        player1Score++;
                        break;
                    case ROCK:
                        player2Score++;
                        break;
                }
                break;
            case PAPER:
                switch (b) {
                    case SCISSORS:
                        player2Score++;
                        break;
                    case ROCK:
                        player1Score++;
                        break;
                }
                break;
            case ROCK:
                switch (b) {
                    case PAPER:
                        player2Score++;
                        break;
                    case SCISSORS:
                        player1Score++;
                        break;
                }
                break;
        }
    }
}
