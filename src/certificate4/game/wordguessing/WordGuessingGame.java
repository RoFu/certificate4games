package certificate4.game.wordguessing;

import com.jme3.app.SimpleApplication;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import java.util.Random;

/**
 * @author Roland Fuller
 */
public class WordGuessingGame extends SimpleApplication implements ActionListener {

    private static final int NUM_LINES = 3; // the number of text lines we want 
    private static final int WELCOME_LINE = 0; // the line to place the welcome message on
    private static final int USER_INPUT_LINE = 1; // the line to display user input on
    private static final int RESULT_LINE = 2; // the line to display the result on
    private BitmapText[] textLines; // An array of BitmapText objects. Each one holds a text string to display
    private String input = ""; // the player's input needs to be initialised to a blank string otherwise it's null
    private final String[] dictionary = {"hello", "goodbye", "cat", "zero"}; // the list of words allowed in the game
    private String guessWord; // the word we're trying to guess
    private int upTo = 0; // remember which letter we have guessed up to

    /**
     * Entry point of the program
     *
     * @param args
     */
    public static void main(String[] args) {
        WordGuessingGame app = new WordGuessingGame();// creates an object of this class's type and...
        app.start(); // passes over control to the inherited engine method, which fires off the game loop
    }

    @Override
    public void simpleInitApp() {
        setDisplayFps(false);
        setDisplayStatView(false);
        setupInput();
        setupRandomWord();
        setupText();
    }

    /**
     * Setup the key mapping
     */
    public void setupInput() {
        inputManager.addListener(this, "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"); // set up the valid actions
        inputManager.addMapping("a", new KeyTrigger(KeyInput.KEY_A)); // pair actions to the correct keys
        inputManager.addMapping("b", new KeyTrigger(KeyInput.KEY_B));
        inputManager.addMapping("c", new KeyTrigger(KeyInput.KEY_C));
        inputManager.addMapping("d", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("e", new KeyTrigger(KeyInput.KEY_E));
        inputManager.addMapping("f", new KeyTrigger(KeyInput.KEY_F));
        inputManager.addMapping("g", new KeyTrigger(KeyInput.KEY_G));
        inputManager.addMapping("h", new KeyTrigger(KeyInput.KEY_H));
        inputManager.addMapping("i", new KeyTrigger(KeyInput.KEY_I));
        inputManager.addMapping("j", new KeyTrigger(KeyInput.KEY_J));
        inputManager.addMapping("k", new KeyTrigger(KeyInput.KEY_K));
        inputManager.addMapping("l", new KeyTrigger(KeyInput.KEY_L));
        inputManager.addMapping("m", new KeyTrigger(KeyInput.KEY_M));
        inputManager.addMapping("n", new KeyTrigger(KeyInput.KEY_N));
        inputManager.addMapping("o", new KeyTrigger(KeyInput.KEY_O));
        inputManager.addMapping("p", new KeyTrigger(KeyInput.KEY_P));
        inputManager.addMapping("q", new KeyTrigger(KeyInput.KEY_Q));
        inputManager.addMapping("r", new KeyTrigger(KeyInput.KEY_R));
        inputManager.addMapping("s", new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping("t", new KeyTrigger(KeyInput.KEY_T));
        inputManager.addMapping("u", new KeyTrigger(KeyInput.KEY_U));
        inputManager.addMapping("v", new KeyTrigger(KeyInput.KEY_V));
        inputManager.addMapping("w", new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("x", new KeyTrigger(KeyInput.KEY_X));
        inputManager.addMapping("y", new KeyTrigger(KeyInput.KEY_Y));
        inputManager.addMapping("z", new KeyTrigger(KeyInput.KEY_Z));
    }

    /**
     * selects a random word from the dictionary array
     */
    public void setupRandomWord() {
        Random random = new Random(); // create a random number generator object
        int wordIndex = random.nextInt(dictionary.length); // pick a random number between 0 and the length of the dictionary array
        guessWord = dictionary[wordIndex]; // grab a random word from the dictionary
        System.out.println(guessWord); // for debugging we print the message to the console
    }

    /**
     * Setup our lines of text
     */
    public void setupText() {
        textLines = new BitmapText[NUM_LINES]; // initialise the array to hold this many objects

        // a number of times we repeat...
        for (int i = 0; i < NUM_LINES; i++) {
            textLines[i] = makeTextLine(i); // creating a text object and adding it to the array
        }

        // Now we use our utility method to set the text of a given line
        setLineText(WELCOME_LINE, "Welcome to my word guessing game...... I'm thinking of a random word. Guess the next letter...");
    }

    /**
     * Create a bitmap text object at a given location
     *
     * @param num the line number so we can place it on the screen
     * @return the created bitmap text object
     */
    public BitmapText makeTextLine(int num) {
        BitmapText line = new BitmapText(guiFont, false); // create the bitmaptext object with a default font in left to right reading order
        line.setSize(guiFont.getCharSet().getRenderedSize()); // set the size of the text for this bitmaptext to the native size of the font
        line.setLocalTranslation(0, settings.getHeight() - (num * line.getSize()), 0); // position the text on the screen according to the resolution and line number
        guiNode.attachChild(line); // we need to attach the bitmaptext to the guiNode so it will be rendered
        return line; // return to caller with the finished bitmaptext
    }

    /**
     * A utility method to safely set the text of a given bitmaptext by line number
     *
     * @param lineNumber the line number to be setting
     * @param text the text to place at this position
     */
    private void setLineText(int lineNumber, String text) {
        // the try block attempts to do something which may be unsafe
        try {
            textLines[lineNumber].setText(text); // note the given index may not exist
        } catch (ArrayIndexOutOfBoundsException aiobe) { // what to do if this particular exception occurred
            System.out.println("Can't set text line " + lineNumber + " because there are only " + textLines.length + " text lines!");
        }
    }

    /**
     * Called when an input to which this listener is registered to is invoked. In this case, we just accumulate the
     * input in the input string as we're building up a number
     *
     * @param name The name of the mapping that was invoked
     * @param isPressed True if the action is "pressed", false otherwise
     * @param tpf The time per frame value. (how long it's been since the last time we were called)
     */
    @Override
    public void onAction(String name, boolean isPressed, float tpf) {
        if (isPressed) {
            checkGuess(name);
        }
    }

    /**
     * Test your guess against the actual number. Display a suitable message.
     *
     * @param guess your guess
     */
    private void checkGuess(String guess) {
        if (guess.equals(guessWord.substring(upTo, upTo + 1))) { // check to see if the first letter we typed is the same as the letter we're trying to now guess
            upTo++; // we can move on to the next letter in the word to be guessed
            setLineText(USER_INPUT_LINE, guessWord.substring(0, upTo));
            setLineText(RESULT_LINE, "You guessed the letter correctly");
            if (upTo >= guessWord.length()) { // when we have guessed all the letters, break out of the loop
                setLineText(RESULT_LINE, "You guessed the word!");
            }
        } else {
            setLineText(RESULT_LINE, "Nope, sorry.");
        }
    }
}
