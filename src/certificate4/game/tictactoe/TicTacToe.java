package certificate4.game.tictactoe;

import com.jme3.app.SimpleApplication;
import com.jme3.collision.CollisionResults;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Ray;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;

/**
 * A game of tic-tac-toe. A 2D array is used to organise a grid of nodes which represent our board squares. This
 * structure is superimposed on the underlying scene graph node tree, as a convenient way to access those nodes in a
 * structure that makes sense for the game.
 *
 * @author Roland Fuller
 */
public class TicTacToe extends SimpleApplication implements ActionListener {

    private static final float SQUARE_SIZE = 0.5f; // how big is a square model
    private static final float TOKEN_SIZE = 0.25f; // how big is a token model
    private static final float MODEL_OFFSET = 0.5f; // how far above the board do the tokens sit
    private static final ColorRGBA PLAYER_ONE_COLOUR = ColorRGBA.Blue;
    private static final ColorRGBA PLAYER_TWO_COLOUR = ColorRGBA.Red;
    private static final String EMPTY = "EMPTY"; // signifies an unoccupied square
    private static final String PLAYER_TOKEN_INFO = "Player Token Info"; // a constant placeholder for user data in the squares
    private Node[][] board = new Node[3][3];// create a 2 dimensional array that imposes structure to the nodes
    private boolean playing = true; // are we still playing or has somebody won?

    /**
     * Entry point of the program
     *
     * @param args
     */
    public static void main(String[] args) {
        TicTacToe app = new TicTacToe();// creates an object of this class's type and...
        app.start(); // passes over control to the inherited engine method, which fires off the game loop
    }

    @Override
    public void simpleInitApp() {
        setDisplayFps(false);
        setDisplayStatView(false);
        setupCamera();
        setupInput();
        makeBoard();
    }

    /**
     * Set up the camera configuration
     */
    private void setupCamera() {
        // place the camera floating a nice location away from the board
        cam.setLocation(new Vector3f(SQUARE_SIZE * (int) Math.ceil(board.length / 2f), SQUARE_SIZE * (int) Math.ceil(board.length / 2f), 4));
        flyCam.setEnabled(false); // don't allow the camera to move around
    }

    /**
     * Setup the key mapping
     */
    private void setupInput() {
        inputManager.setCursorVisible(true); // we want the mouse cursor in this game.
        inputManager.addListener(this, "click"); // set up the valid actions
        inputManager.addMapping("click", new MouseButtonTrigger(MouseInput.BUTTON_LEFT)); // map action to a key
    }

    /**
     * Create all the squares on the board
     */
    private void makeBoard() {
        ColorRGBA colour;
        // nested loop creates a grid
        for (int x = 0; x < 3; x++) { // go three across
            for (int y = 0; y < 3; y++) { // and three up
                if ((x + y) % 2 == 0) { // odd/even squares alternate colour
                    colour = ColorRGBA.Gray;
                } else {
                    colour = ColorRGBA.DarkGray;
                }
                makeSquare(x, y, colour); // use the iteration as a basis for the positioning
            }
        }
    }

    /**
     * make a single square on the board
     *
     * @param x the x location which also corresponds to the coordinate in the array
     * @param y the y location which also corresponds to the coordinate in the array
     * @param colour the colour of the square - purely cosmetic
     */
    private void makeSquare(int x, int y, ColorRGBA colour) {
        Node node = new Node(); // create a container
        node.setUserData(PLAYER_TOKEN_INFO, EMPTY); // add a custom value to the node which specifies which token is here
        board[x][y] = node; // add the node to the array to impose the grid structure arrangement of the nodes
        node.setLocalTranslation(new Vector3f(x, y, 0)); // place in space
        Geometry geom = new Geometry("square", new Box(SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE));

        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", colour);
        geom.setMaterial(mat);

        node.attachChild(geom); // add the model to the container
        rootNode.attachChild(node);
    }

    /**
     * Create the token model
     *
     * @param parent which node to attach the model to
     * @param colour the colour of the material of this token
     */
    private void makeToken(Node parent, ColorRGBA colour) {
        Geometry geom = new Geometry("token", new Box(TOKEN_SIZE, TOKEN_SIZE, TOKEN_SIZE)); // create the model
        geom.setLocalTranslation(new Vector3f(0, 0, MODEL_OFFSET)); // float above the square this much (relative)

        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", colour);
        geom.setMaterial(mat);

        parent.attachChild(geom); // attach to the square node not the root node
    }

    /**
     * Add a token to the board
     *
     * @param node which node to add to
     * @param colour which colour the token is
     * @return whether it was successful
     */
    private boolean placeToken(Node node, ColorRGBA colour) {
        if (node.getChildren().size() < 2) { // if the node is "empty" e.g has only a square, no token yet
            node.setUserData(PLAYER_TOKEN_INFO, colour.toString()); // change the token info of the node to signify what's there
            makeToken(node, colour); // create the model to sit here
            check(); // check the game rules
            return true;
        } else {
            return false;
        }
    }

    /**
     * Called when an input to which this listener is registered to is invoked. In this case, we just accumulate the
     * input in the input string as we're building up a number
     *
     * @param name The name of the mapping that was invoked
     * @param isPressed True if the action is "pressed", false otherwise
     * @param tpf The time per frame value. (how long it's been since the last time we were called)
     */
    @Override
    public void onAction(String name, boolean isPressed, float tpf) {
        if (playing) {
            if (isPressed) { // only on the down click
                Vector2f click2d = inputManager.getCursorPosition(); // find out where the mouse cursor is on the screen resolution
                Vector3f click3d = cam.getWorldCoordinates(click2d, 0f); // find out where that corresponds to in 3d space
                Vector3f dir = cam.getWorldCoordinates(click2d, 1f).subtractLocal(click3d).normalizeLocal(); // create a vector from the camera pointing in that direction
                Ray ray = new Ray(click3d, dir); // cast a ray into the scene from the camera in the direction of the clicked point
                CollisionResults collisionResults = new CollisionResults(); // create a container for storing collisions
                rootNode.collideWith(ray, collisionResults); // perform the actual collision calculation

                if (collisionResults.size() > 0) { // if we clicked on any geometry
                    Node node = collisionResults.getClosestCollision().getGeometry().getParent(); // only care about the first collision
                    if (placeToken(node, PLAYER_ONE_COLOUR)) { // only if it was successful do we:
                        doComputerMove(); // allow the computer to make a move in turn
                    }
                }
            }
        }
    }

    /**
     * Generate a random move for the computer to place This AI is not very smart. It just looks at which squares are
     * free and places in the first available slot.
     */
    private void doComputerMove() {
        if (playing) { // otherwise the computer has a go after the player has won
            for (Spatial spatial : rootNode.getChildren()) { // look at all the spatials in the root tree
                if (((Node) spatial).getChildren().size() < 2) { // if one has available slot (only a square model, no token)
                    placeToken((Node) spatial, PLAYER_TWO_COLOUR); // place my token here
                    return; // finished with my move
                }
            }
        }
    }

    /**
     * Check all the possibilities for winning the game.
     */
    private void check() {
        checkWinner(vertical(), "vertical");
        checkWinner(horizontal(), "horizontal");
        checkWinner(diagonalUp(), "diagonalUp");
        checkWinner(diagonalDown(), "diagonalDown");
    }

    /**
     * See if one of the check results was an actual player. If so, they win.
     *
     * @param checkResult the result of individual direction checks.
     */
    private void checkWinner(String checkResult, String check) {
        if (!checkResult.equals(EMPTY)) {
            System.out.println("Winner is: " + checkResult + " on " + check);
            playing = false; // don't allow any further moves to be made.
        }
    }

    /**
     * Go through all of the vertical columns to see if there is an unbroken string of the same token. It goes through
     * coordinates in the order: (0,0),(0,1),(0,2),(1,0),(1,1),(1,2),(2,0),(2,1),(2,2)
     *
     * @return the token that won because of this, otherwise empty
     */
    private String vertical() {
        for (int x = 0; x < board.length; x++) { // for every column
            boolean complete = true; // assume a column will have a winning streak by default
            String firstToken = board[x][0].getUserData(PLAYER_TOKEN_INFO); // grab the token at the bottom
            for (int y = 0; y < board.length; y++) { // go through each of the rows
                String nextToken = board[x][y].getUserData(PLAYER_TOKEN_INFO); // grab the token from the current column/row
                if (!firstToken.equals(nextToken)) { // if they aren't the same token
                    complete = false; // we found a break in the chain
                    continue; // go straight to the next iteration of the loop for efficiency's sake
                }
            }
            // If we made it through all columns without being falsified and all tokens are the same
            if (complete && !firstToken.equals(EMPTY)) {
                return firstToken; // tell the caller what was the name of the token which was unbroken somewhere vertically
            }
        }
        return EMPTY;
    }

    /**
     * Go through all of the horizontal columns to see if there is an unbroken string of the same token. This method is
     * very similar to the vertical method and features a lot of duplication. It would be possible to merge these
     * methods together (and perhaps even the diagonals!) if you pass in some parameters controlling which direction to
     * move the x and y in and which is the inner/outer loop... Or perhaps origin/destination coordinates. The method
     * itself would likely become much more complex and may require breaking up into a few helper methods, but it would
     * then be able to handle looking at lines through any 2D array and be highly re-usable as a library class!
     * (CHALLENGE)
     *
     * @return the token that won because of this, otherwise empty
     */
    private String horizontal() {
        for (int x = 0; x < board.length; x++) {
            boolean complete = true;
            String firstToken = board[0][x].getUserData(PLAYER_TOKEN_INFO);
            for (int y = 0; y < board.length; y++) {
                String nextToken = board[y][x].getUserData(PLAYER_TOKEN_INFO);
                if (!firstToken.equals(nextToken)) {
                    complete = false;
                    continue;
                }
            }
            if (complete && !firstToken.equals(EMPTY)) {
                return firstToken;
            }
        }
        return EMPTY;
    }

    /**
     * Go through the diagonals to check for a win. (bottom left to top right) This one just steps through: (0,0),(1,1),
     * (2,2).
     *
     * @return the token that won because of this, otherwise empty
     */
    private String diagonalUp() {
        boolean complete = true;
        int x = 0;
        int y = 0;
        String firstToken = board[x][y].getUserData(PLAYER_TOKEN_INFO);
        for (x = x + 1, y = y + 1; x < board.length; x++, y++) {
            String nextToken = board[x][y].getUserData(PLAYER_TOKEN_INFO);
            if (!firstToken.equals(nextToken)) {
                complete = false;
                break;
            }
        }
        if (complete && !firstToken.equals(EMPTY)) {
            return firstToken;
        }
        return EMPTY;
    }

    /**
     * Go through the diagonals to check for a win. (top left to bottom right)
     *
     * This method requires going up on the y and simultaneously down on the x. So the iterations result in coordinates:
     * (2,0),(1,1),(0,2)
     *
     * @return the token that won because of this, otherwise empty
     */
    private String diagonalDown() {
        boolean complete = true;
        int x = board.length - 1;
        int y = 0;
        String firstToken = board[x][y].getUserData(PLAYER_TOKEN_INFO);
        for (x = x - 1, y = y + 1; x >= 0; x--, y++) {
            String nextToken = board[x][y].getUserData(PLAYER_TOKEN_INFO);
            if (!firstToken.equals(nextToken)) {
                complete = false;
                break;
            }
        }
        if (complete && !firstToken.equals(EMPTY)) {
            return firstToken;
        }
        return EMPTY;
    }
}
