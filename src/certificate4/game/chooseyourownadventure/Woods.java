package certificate4.game.chooseyourownadventure;

import certificate4.game.chooseyourownadventure.ChooseYourOwnAdventure.Status;

/**
 * The woods is the end game location
 *
 * @author Roland Fuller
 */
public class Woods extends Location {

    /**
     * Constructor method just passes the name and description on to the super class (parent class Location we extend
     * from)
     *
     * @param name
     * @param description
     */
    public Woods(String name, String description) {
        super(name, description);
    }

    /**
     * Because this location is special we override it, replacing the inherited version of this method from the parent.
     *
     * @return the win state, as this is the end location
     */
    @Override
    public Status visit() {
        System.out.println(description); // give a little message
        return Status.WIN;
    }
}
