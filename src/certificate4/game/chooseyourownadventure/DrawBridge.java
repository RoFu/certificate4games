package certificate4.game.chooseyourownadventure;

/**
 * The drawbridge inherits from the basic location class
 *
 * @author Roland Fuller
 */
public class DrawBridge extends Location {

    // Used to specify whether the door is open or not yet.
    private boolean open;

    /**
     * Set up the drawbridge
     *
     * @param name
     * @param description
     */
    public DrawBridge(String name, String description) {
        super(name, description);
        open = false;
    }

    /**
     * There is something special about this class: using specifically the key item here will change its state and
     * display a message (ideally user interface code shouldn't be here, though)
     *
     * @param item
     */
    @Override
    public void useItem(String item) {
        if (item.equals("key")) {
            open = true;
            System.out.println("Clunk! The drawbridge unlocks and comes slamming into the ground with an all mighty CRASH!!!");
        }
    }

    /**
     * This location's east direction is guarded by the open status flag. i.e there is no location to the east if we
     * haven't already used the key to open the door.
     *
     * @return
     */
    @Override
    public Location getEast() {
        if (open) {
            return east;
        } else {
            return null;
        }
    }
}
