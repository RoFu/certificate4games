package certificate4.game.chooseyourownadventure;

/**
 * Represents the player of the game all their data and behaviours
 *
 * @author Roland Fuller
 */
public class Player {

    private int intelligence;
    private int health;
    private String item; // the item I'm holding - just a string

    /**
     * Constructor method
     */
    public Player() {
        intelligence = 105; // hard-code some values
        health = 50; // because we don't have a character creation process
    }

    /**
     * Getter Method
     *
     * @return the intelligence variable
     */
    public int getIntelligence() {
        return intelligence;
    }

    /**
     * Getter Method
     *
     * @return the health variable
     */
    public int getHealth() {
        return health;
    }

    /**
     * Getter Method
     *
     * @return the item variable
     */
    public String getItem() {
        return item;
    }

    /**
     * Setter Method
     *
     * @param item give the player an item Currently only one item allowed - setting a new one will overwrite the old
     */
    public void setItem(String item) {
        this.item = item;
    }

}
