package certificate4.game.chooseyourownadventure;

import certificate4.game.chooseyourownadventure.ChooseYourOwnAdventure.Status;

/**
 * Describes a location data and functionality
 *
 * @author Roland Fuller
 */
public class Location {

    protected String name;
    protected String description;
    protected String item;
    // These variables refer to other location objects in the map
    protected Location north, south, east, west;

    /**
     * Constructor sets up the location object
     *
     * @param name pass in the name
     * @param description pass in the description
     */
    public Location(String name, String description) {
        this.name = name;
        this.description = description;
    }

    /**
     * Getter Method
     *
     * @return the name variable
     */
    public String getName() {
        return name;
    }

    /**
     * Getter Method
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Getter Method
     *
     * @return the north location
     */
    public Location getNorth() {
        return north;
    }

    /**
     * Getter Method
     *
     * @return the south location
     */
    public Location getSouth() {
        return south;
    }

    /**
     * Getter Method
     *
     * @return the east location
     */
    public Location getEast() {
        return east;
    }

    /**
     * Getter Method
     *
     * @return the west location
     */
    public Location getWest() {
        return west;
    }

    /**
     * Setter Method
     *
     * @param north - remember this location as north
     */
    public void setNorth(Location north) {
        this.north = north;
    }

    /**
     * Setter Method
     *
     * @param south - remember this location as south
     */
    public void setSouth(Location south) {
        this.south = south;
    }

    /**
     * Setter Method
     *
     * @param east - remember this location as east
     */
    public void setEast(Location east) {
        this.east = east;
    }

    /**
     * Setter Method
     *
     * @param west - remember this location as west
     */
    public void setWest(Location west) {
        this.west = west;
    }

    /**
     * Setter Method
     *
     * @param west - remember the item given in the location
     */
    public void setItem(String item) {
        this.item = item;
    }

    /**
     * Getter Method
     *
     * @returnthe item stored in this location (just a string)
     */
    public String getItem() {
        return item;
    }

    /**
     * Getter Method
     *
     * @return a string representing the item
     */
    public String takeItem() {
        String result = this.item;
        this.item = null;
        return result;
    }

    /**
     * This method does nothing by default - child classes should override it if they want to do something specific
     * Using an item at this location.
     *
     * @param item the item to use
     */
    public void useItem(String item) {
    }

    /**
     * Visit just means the player has entered this location It is only used currently to specify whether that should
     * result in the game status changing
     *
     * @return
     */
    public Status visit() {
        return Status.PLAYING;
    }
}
