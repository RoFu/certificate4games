package certificate4.game.chooseyourownadventure;

import com.jme3.app.SimpleApplication;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;

/**
 * A text based adventure game.
 *
 * @author Roland Fuller
 */
public class ChooseYourOwnAdventure extends SimpleApplication implements ActionListener {

    /**
     * Reflects the state of the game. We could add states for losing etc
     */
    public enum Status {
        PLAYING, WIN
    }
    private static final int NUM_LINES = 10; // the number of text lines we want 
    private static final int USER_INPUT_LINE = 0; // the line to display user input on
    private static final int RESULT_LINE = 1; // the line to display the result on
    private BitmapText[] textLines; // An array of BitmapText objects. Each one holds a text string to display
    private String input = ""; // the player's input needs to be initialised to a blank string otherwise it's null
    private Player player; // an object to represent the player
    private Location place; // an object to represent the current location the player is in
    private Status status; // a variable which stores the status of the game

    /**
     * The main method instantiates a ChooseYourOwnAdventure object and then
     * passed over execution control to it via the play method.
     *
     * @param args
     */
    public static void main(String[] args) {
        ChooseYourOwnAdventure cyoa = new ChooseYourOwnAdventure();
        cyoa.start();
    }

    /**
     * Setup all the bits of the game here.
     */
    @Override
    public void simpleInitApp() {
        setDisplayFps(false);
        setDisplayStatView(false);
        setupInput();
        setupText();
        setupMap();
        player = new Player(); // make a player object and store its location in member variable
        status = Status.PLAYING; // set the initial game status
        printStatus();
    }

    /**
     * Setup the key mapping
     */
    public void setupInput() {
        inputManager.addListener(this, "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "Enter", " ", "back"); // set up the valid actions
        inputManager.addMapping("0", new KeyTrigger(KeyInput.KEY_0)); // pair actions to the correct keys
        inputManager.addMapping("1", new KeyTrigger(KeyInput.KEY_1));
        inputManager.addMapping("2", new KeyTrigger(KeyInput.KEY_2));
        inputManager.addMapping("3", new KeyTrigger(KeyInput.KEY_3));
        inputManager.addMapping("4", new KeyTrigger(KeyInput.KEY_4));
        inputManager.addMapping("5", new KeyTrigger(KeyInput.KEY_5));
        inputManager.addMapping("6", new KeyTrigger(KeyInput.KEY_6));
        inputManager.addMapping("7", new KeyTrigger(KeyInput.KEY_7));
        inputManager.addMapping("8", new KeyTrigger(KeyInput.KEY_8));
        inputManager.addMapping("9", new KeyTrigger(KeyInput.KEY_9));
        inputManager.addMapping("a", new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping("b", new KeyTrigger(KeyInput.KEY_B));
        inputManager.addMapping("c", new KeyTrigger(KeyInput.KEY_C));
        inputManager.addMapping("d", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("e", new KeyTrigger(KeyInput.KEY_E));
        inputManager.addMapping("f", new KeyTrigger(KeyInput.KEY_F));
        inputManager.addMapping("g", new KeyTrigger(KeyInput.KEY_G));
        inputManager.addMapping("h", new KeyTrigger(KeyInput.KEY_H));
        inputManager.addMapping("i", new KeyTrigger(KeyInput.KEY_I));
        inputManager.addMapping("j", new KeyTrigger(KeyInput.KEY_J));
        inputManager.addMapping("k", new KeyTrigger(KeyInput.KEY_K));
        inputManager.addMapping("l", new KeyTrigger(KeyInput.KEY_L));
        inputManager.addMapping("m", new KeyTrigger(KeyInput.KEY_M));
        inputManager.addMapping("n", new KeyTrigger(KeyInput.KEY_N));
        inputManager.addMapping("o", new KeyTrigger(KeyInput.KEY_O));
        inputManager.addMapping("p", new KeyTrigger(KeyInput.KEY_P));
        inputManager.addMapping("q", new KeyTrigger(KeyInput.KEY_Q));
        inputManager.addMapping("r", new KeyTrigger(KeyInput.KEY_R));
        inputManager.addMapping("s", new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping("t", new KeyTrigger(KeyInput.KEY_T));
        inputManager.addMapping("u", new KeyTrigger(KeyInput.KEY_U));
        inputManager.addMapping("v", new KeyTrigger(KeyInput.KEY_V));
        inputManager.addMapping("w", new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("x", new KeyTrigger(KeyInput.KEY_X));
        inputManager.addMapping("y", new KeyTrigger(KeyInput.KEY_Y));
        inputManager.addMapping("z", new KeyTrigger(KeyInput.KEY_Z));
        inputManager.addMapping("Enter", new KeyTrigger(KeyInput.KEY_RETURN));
        inputManager.addMapping(" ", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addMapping("back", new KeyTrigger(KeyInput.KEY_BACK));
    }

    /**
     * Setup our lines of text
     */
    public void setupText() {
        textLines = new BitmapText[NUM_LINES]; // initialise the array to hold this many objects

        // a number of times we repeat...
        for (int i = 0; i < NUM_LINES; i++) {
            textLines[i] = makeTextLine(i); // creating a text object and adding it to the array
        }
    }

    /**
     * Create a bitmap text object at a given location
     *
     * @param num the line number so we can place it on the screen
     * @return the created bitmap text object
     */
    public BitmapText makeTextLine(int num) {
        BitmapText line = new BitmapText(guiFont, false); // create the bitmaptext object with a default font in left to right reading order
        line.setSize(guiFont.getCharSet().getRenderedSize()); // set the size of the text for this bitmaptext to the native size of the font
        line.setLocalTranslation(0, settings.getHeight() - (num * line.getSize()), 0); // position the text on the screen according to the resolution and line number
        guiNode.attachChild(line); // we need to attach the bitmaptext to the guiNode so it will be rendered
        return line; // return to caller with the finished bitmaptext
    }

    /**
     * A utility method to safely set the text of a given bitmaptext by line
     * number
     *
     * @param lineNumber the line number to be setting
     * @param text the text to place at this position
     */
    private void setLineText(int lineNumber, String text) {
        // the try block attempts to do something which may be unsafe
        try {
            textLines[lineNumber].setText(text); // note the given index may not exist
        } catch (ArrayIndexOutOfBoundsException aiobe) { // what to do if this particular exception occurred
            System.out.println("Can't set text line " + lineNumber + " because there are only " + textLines.length + " text lines!");
        }
    }

    /**
     * Utility method to clear all text on the screen.
     */
    private void clearTextLines() {
        for (int i = 0; i < NUM_LINES; i++) {
            setLineText(i, "");
        }
    }

    /**
     * Setup the map objects
     */
    private void setupMap() {
        // set up a hard-coded map. Here we make individual objects representing different locations.
        // The constructors are called, setting the name and descriptions
        Location woods = new Woods("the woods", "A long winding path towards home!");
        Location drawbridge = new DrawBridge("the drawbridge", "A solid wooden platform.");
        Location throneRoom = new Location("throne room", "A sprawling hallway with a throne at the end.");
        Location turrets = new Location("turrets", "A row of cannons lining a hefty stone wall with a clear view of the surrounding terrain.");
        Location dungeon = new Location("dungeon", "A bone laden dank pit of despair you must escape from.");
        Location tower = new Location("tower", "A scary creaky old tower.");

        // link the location objects together
        dungeon.setNorth(turrets);
        turrets.setSouth(dungeon);
        turrets.setNorth(tower);
        tower.setSouth(turrets);
        turrets.setEast(throneRoom);
        throneRoom.setWest(turrets);
        throneRoom.setEast(drawbridge);
        drawbridge.setWest(throneRoom);
        drawbridge.setEast(woods);

        tower.setItem("key");

        place = dungeon; // set starting location
    }

    /**
     * Called when an input to which this listener is registered to is invoked.
     * In this case, we just accumulate the input in the input string as we're
     * building up a command name to follow.
     *
     * @param name The name of the mapping that was invoked
     * @param isPressed True if the action is "pressed", false otherwise
     * @param tpf The time per frame value. (how long it's been since the last
     * time we were called)
     */
    @Override
    public void onAction(String name, boolean isPressed, float tpf) {
        if (isPressed) {
            if (name.equals("Enter")) { // submit number
                try {
                    String[] split = input.split(" "); // split apart string on each space
                    doCommand(split); // check if it was correct
                } catch (NumberFormatException nfe) {
                    setLineText(RESULT_LINE, "Unknown command.");
                }
                input = ""; //reset our guess
            } else if (name.equals("back")) {
                if (input.length() > 0) {
                    input = input.substring(0, input.length() - 1);
                    setLineText(USER_INPUT_LINE, input);
                }
            } else {// accumulate number
                input += name;
                setLineText(USER_INPUT_LINE, input);
            }
        }
    }

    /**
     * Process the player's command.
     */
    private void doCommand(String[] input) {
        switch (input[0]) {
            case "move":
                move(input[1]); // second word is the move parameter i.e which direction to move in
                break;
            case "take":
                player.setItem(place.takeItem()); // give the player that location's item
                break;
            case "use":
                place.useItem(player.getItem()); // use player's item on the location
                break;
        }
        printStatus();
    }

    /**
     * Move the player to another location
     *
     * @param direction the direction to move in
     */
    public void move(String direction) {
        switch (direction) {
            case "north":
                if (place.getNorth() != null) {
                    place = place.getNorth();
                }
                break;
            case "south":
                if (place.getSouth() != null) {
                    place = place.getSouth();
                }
                break;
            case "east":
                if (place.getEast() != null) {
                    place = place.getEast();
                }
                break;
            case "west":
                if (place.getWest() != null) {
                    place = place.getWest();
                }
                break;
        }
        status = place.visit();
    }

    /**
     * Print out on the console the details of the game state including player's
     * state and what locations are available from current locaiton
     */
    private void printStatus() {
        clearTextLines();
        switch (status) {
            case PLAYING:
                int lineNumber = 2;
                setLineText(0, "You are currently in " + place.getName() + ". " + place.getDescription());
                setLineText(1, "You have " + player.getHealth() + " health");

                if (player.getItem() != null) {
                    setLineText(lineNumber, "You have a " + player.getItem());
                    lineNumber++;
                }

                if (place.getNorth() != null) {
                    setLineText(lineNumber, "to the North is " + place.getNorth().getName());
                    lineNumber++;
                }
                if (place.getSouth() != null) {
                    setLineText(lineNumber, "to the South is " + place.getSouth().getName());
                    lineNumber++;
                }
                if (place.getEast() != null) {
                    setLineText(lineNumber, "to the East is " + place.getEast().getName());
                    lineNumber++;
                }
                if (place.getWest() != null) {
                    setLineText(lineNumber, "to the West is " + place.getWest().getName());
                    lineNumber++;
                }
                if (place.getItem() != null) {
                    setLineText(lineNumber, "Here there is " + place.getItem());
                    lineNumber++;
                }
                break;
            case WIN:
                setLineText(0, "You're home free! ");
                break;
        }
    }
}
