package pracday.game;

import com.jme3.effect.ParticleEmitter;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;

/**
 *
 * @author Roland Fuller
 */
public class ParticleFader extends AbstractControl {

    private ParticleEmitter emitter;
    private float timer;
    private float frequency;
    private static final float FADE_SPEED = 15f;

    @Override
    public void setSpatial(Spatial spatial) {
        super.setSpatial(spatial);
        // get a handle on the particle emitter
        emitter = (ParticleEmitter) spatial;
        // figure out how fast to reduce particles
        frequency = emitter.getMaxNumParticles() / FADE_SPEED;
    }

    @Override
    protected void controlUpdate(float tpf) {
        timer += tpf;
        // count up the timer, if it's hit the time limit;
        if (timer >= frequency) {
            timer = 0;
            // reset to start timing again and reduce the number of particles in effect
            int amt = emitter.getParticles().length - 1;
            emitter.setNumParticles(amt);
            if (emitter.getNumVisibleParticles() <= 0) {
                // if we've run out of particles, remove the effect
                spatial.removeFromParent();
            }
        }
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }
}
