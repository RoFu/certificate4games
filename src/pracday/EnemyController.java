package pracday.game;

import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;

/**
 *
 * @author Roland Fuller
 */
public class EnemyController extends AbstractControl {

    private static final float ENEMY_SPEED = 10;
    private BetterCharacterControl betterCharacterControl;

    @Override
    public void setSpatial(Spatial spatial) {
        super.setSpatial(spatial);
        // obtain character physics control from enemy model
        betterCharacterControl = spatial.getControl(BetterCharacterControl.class);
    }

    @Override
    protected void controlUpdate(float tpf) {
        // calculate direction from my position to the center point
        Vector3f direction = Vector3f.ZERO.subtract(spatial.getLocalTranslation());
        direction.y = 0;
        direction.normalizeLocal();
        // walk and face that direction
        betterCharacterControl.setWalkDirection(direction.mult(ENEMY_SPEED));
        betterCharacterControl.setViewDirection(direction);
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }
}
