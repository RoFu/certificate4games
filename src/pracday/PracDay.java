package pracday;

// We need a bunch of classes from the libraries
import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.LoopMode;
import com.jme3.app.SimpleApplication;
import com.jme3.audio.AudioNode;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.collision.shapes.SphereCollisionShape;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.collision.CollisionResults;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.SceneGraphVisitor;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Sphere;
import com.jme3.texture.Texture;
import com.jme3.util.SkyFactory;
import java.util.Random;
import pracday.game.EnemyController;
import pracday.game.ParticleFader;

/**
 * A basic FPS game with a maze, enemies with simple AI, a ray casting weapon and an exit
 *
 * @author Roland Fuller
 */
public class PracDay extends SimpleApplication implements PhysicsCollisionListener {

    // Many many constant variables which affect the game play
    private final int NUM_ENEMIES = 50;
    private final String CANNON_BALL_NAME = "CannonBall";
    private final String ENEMY_CONTAINER_NAME = "EnemyContainer";
    private final String WALL_NAME = "Wall";
    private final String TEAPOT_TNAME = "TEAPOT!";
    private final String LOSER_MSG = "YOU LOSE!!!!!";
    private final float SHOOT_REFIRE_TIME = 0.25f;
    private final float SHOOT_ALT_REFIRE_TIME = 1f;
    private final float CANNON_BALL_SPEED = 250;
    private final float MINIMUM_SPAWN_DISTANCE = 50;
    public final Vector3f DEBRIS_INITIAL_VELOCITY = new Vector3f(0, 20, 0);
    public final Vector3f DEBRIS_GRAVITY = new Vector3f(0, 9.8f, 0);
    public final float DEBRIS_VELOCITY_VARIATION = 1f;
    public final int DEBRIS_ROTATE_SPEED = 4;
    public final int DEBRIS_START_SIZE = 4;
    public final int DEBRIS_NUM_PARTICLES = 100;
    public final Vector3f LIGHT_DIRECTION = new Vector3f(-0.1f, -0.7f, -1.0f);
    public final ColorRGBA LIGHT_COLOUR = ColorRGBA.White;
    public final Vector3f CAMERA_LOCATION = new Vector3f(0, 50, 150);
    public final int TEXT_SCALE = 5;
    public final ColorRGBA FLOOR_COLOUR = ColorRGBA.White;
    public final ColorRGBA FLOOR_SPECULAR = ColorRGBA.White;
    public final float FLOOR_SHININESS = 64f;
    public final int BLOCK_SPACING = 50;
    public final ColorRGBA BLOCK_COLOUR = ColorRGBA.White;
    public final ColorRGBA BLOCK_SPECULAR = ColorRGBA.White;
    public final float BLOCK_SHININESS = 64f;
    ////////////// Don't change the below variables too much///////////////
    private int enemyCount = 0;
    private int kills = 0;
    private BitmapText killText;
    private float shootTimer;
    private float shootAltTimer;
    private AudioNode audio_gun;
    private AudioNode audio_bang;
    private AudioNode audio_nature;
    private BulletAppState bulletAppState;

    // entry point to the program
    public static void main(String[] args) {
        PracDay app = new PracDay();
        app.start();
    }
// Application is about to start, run some setup

    @Override
    public void simpleInitApp() {
        cam.setLocation(CAMERA_LOCATION);
        flyCam.setMoveSpeed(0);
        stateManager.attach(bulletAppState = new BulletAppState());
        // re-enable this line to see physics shapes
        //bulletAppState.setDebugEnabled(true);
        bulletAppState.getPhysicsSpace().addCollisionListener(this);
        initInput();
        initAudio();
        initGUI();
        initCrossHairs();
        makeWorld();
    }

    // get a random location
    private Vector3f getRandomVector(float scale, float height) {
        Random r = new Random();
        float x = r.nextBoolean() ? r.nextFloat() + 0.1f : -r.nextFloat() - 0.1f;
        float y = height;
        float z = r.nextBoolean() ? r.nextFloat() + 0.1f : -r.nextFloat() - 0.1f;
        return new Vector3f(x * scale, y, z * scale);
    }

    // set up the GUI text
    private void initGUI() {
        guiNode.detachAllChildren();
        guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
        killText = new BitmapText(guiFont, false);
        killText.setSize(guiFont.getCharSet().getRenderedSize() * TEXT_SCALE);
        killText.setLocalTranslation(settings.getWidth() / 2 - killText.getLineWidth() / 2, killText.getLineHeight(), 0);
        guiNode.attachChild(killText);
    }

    // create a single light source for the whole world
    private void initLighting() {
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(LIGHT_DIRECTION);
        sun.setColor(LIGHT_COLOUR);
        rootNode.addLight(sun);
    }

    // create crosshair image
    protected void initCrossHairs() {
        setDisplayStatView(false);
        guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
        BitmapText ch = new BitmapText(guiFont, false);
        ch.setSize(guiFont.getCharSet().getRenderedSize() * 2);
        ch.setText("+");
        ch.setLocalTranslation(settings.getWidth() / 2 - ch.getLineWidth() / 2, settings.getHeight() / 2 + ch.getLineHeight() / 2, 0);
        guiNode.attachChild(ch);
    }

    // load in some audio files
    private void initAudio() {
        audio_gun = new AudioNode(assetManager, "Sound/Effects/Gun.wav");
        audio_gun.setPositional(false);
        audio_gun.setLooping(false);
        audio_gun.setVolume(2);
        rootNode.attachChild(audio_gun);

        // OPTION 1
        //audio_bang = new AudioNode(assetManager, "Sound/Effects/Beep.ogg");
        // OPTION 1
        // OPTION 2
        //audio_bang = new AudioNode(assetManager, "Sound/Effects/Foot steps.ogg");
        // OPTION 2
        // OPTION 3
        //audio_bang = new AudioNode(assetManager, "Sound/Effects/kick.wav");
        // OPTION 3
        // OPTION 4
        audio_bang = new AudioNode(assetManager, "Sound/Effects/Bang.wav");
        // OPTION 4
        audio_bang.setPositional(false);
        audio_bang.setLooping(false);
        audio_bang.setVolume(2);
        rootNode.attachChild(audio_bang);

        // OPTION 1
        audio_nature = new AudioNode(assetManager, "Sound/Environment/Nature.ogg");
        // OPTION 1

        // OPTION 2
        //audio_nature = new AudioNode(assetManager, "Sound/Environment/Ocean Waves.ogg");
        // OPTION 2
        // OPTION 3
        //audio_nature = new AudioNode(assetManager, "Sound/Environment/River.ogg");
        // OPTION 3
        audio_nature.setLooping(true);
        audio_nature.setPositional(false);
        audio_nature.setVolume(1);
        rootNode.attachChild(audio_nature);
        audio_nature.play();
    }

    // get rid of a game object from scene graph and physics engine
    private void removeObject(Spatial object) {
        bulletAppState.getPhysicsSpace().removeAll(object);
        object.removeFromParent();
    }

    // get rid of an enemy 
    private void killEnemey(Spatial enemy) {
        makeFire(enemy.getLocalTranslation());
        makeDebris(enemy.getLocalTranslation());
        removeObject(enemy);
        audio_bang.playInstance();
        enemyCount--;
        kills++;
    }

    // when something is in range of a cannonball blast
    private void explode(Spatial object) {
        if (object.getName() != null) {
            if (object.getName().equals(ENEMY_CONTAINER_NAME)) {
                killEnemey(object);
            } else if (object.getName().equals(WALL_NAME)) {
                removeObject(object);
                audio_bang.playInstance();
            }
        }
    }

    // utility to determine which way around collision happened
    private Spatial resolve(Spatial a, Spatial b, String name) {
        if (a.getName().equals(name)) {
            return a;
        } else if (b.getName().equals(name)) {
            return b;
        } else {
            return null;
        }
    }

    // check everything to see if in range of a cannonball blast and explode it if so
    public void explosion(final Vector3f location) {
        rootNode.breadthFirstTraversal(new SceneGraphVisitor() {
            public final int CANNON_EXPLOSION_RADIUS = 25;

            public void visit(Spatial spatial) {
                if (location.distance(spatial.getLocalTranslation()) < CANNON_EXPLOSION_RADIUS) {
                    explode(spatial);
                }
            }
        });
    }

    // create a secondary particle effect
    public void makeDebris(Vector3f location) {
        ParticleEmitter debris = new ParticleEmitter("Debris", ParticleMesh.Type.Triangle, DEBRIS_NUM_PARTICLES);
        debris.addControl(new ParticleFader());
        debris.setLocalTranslation(location);
        Material debris_mat = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");

        // OPTION 1
        debris_mat.setTexture("Texture", assetManager.loadTexture("Effects/Explosion/Debris.png"));
        debris.setImagesX(3);
        debris.setImagesY(3);
        // OPTION 1

        // OPTION 2
        //debris_mat.setTexture("Texture", assetManager.loadTexture("Effects/Explosion/flame.png"));
        //debris.setImagesX(2);
        //debris.setImagesY(2);
        // OPTION 2
        // OPTION 3
        //debris_mat.setTexture("Texture", assetManager.loadTexture("Effects/Explosion/flash.png"));
        //debris.setImagesX(2);
        //debris.setImagesY(2);
        // OPTION 3
        // OPTION 4
        //debris_mat.setTexture("Texture", assetManager.loadTexture("Effects/Explosion/roundspark.png"));
        //debris.setImagesX(2);
        //debris.setImagesY(2);
        // OPTION 4
        // OPTION 5
        //debris_mat.setTexture("Texture", assetManager.loadTexture("Effects/Explosion/shockwave.png"));
        // OPTION 5
        // OPTION 6
        //debris_mat.setTexture("Texture", assetManager.loadTexture("Effects/Explosion/smoketrail.png"));
        //debris.setImagesX(1);
        //debris.setImagesY(3);
        // OPTION 6
        // OPTION 7
        //debris_mat.setTexture("Texture", assetManager.loadTexture("Effects/Explosion/spark.png"));
        // OPTION 7
        // OPTION 8
        //debris_mat.setTexture("Texture", assetManager.loadTexture("Effects/Smoke/Smoke.png"));
        // OPTION 8
        debris.setMaterial(debris_mat);
        debris.setStartSize(DEBRIS_START_SIZE);
        debris.setRotateSpeed(DEBRIS_ROTATE_SPEED);
        debris.setSelectRandomImage(true);
        debris.getParticleInfluencer().setInitialVelocity(DEBRIS_INITIAL_VELOCITY);
        debris.setStartColor(ColorRGBA.White);
        debris.setGravity(DEBRIS_GRAVITY);
        debris.getParticleInfluencer().setVelocityVariation(DEBRIS_VELOCITY_VARIATION);
        rootNode.attachChild(debris);
        debris.emitAllParticles();
    }

    // create a firey particle effect
    public void makeFire(Vector3f location) {
        ParticleEmitter fire
                = new ParticleEmitter("Emitter", ParticleMesh.Type.Triangle, 100);
        fire.setLocalTranslation(location);
        Material mat_red = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
        mat_red.setTexture("Texture", assetManager.loadTexture("Effects/Explosion/flame.png"));
        fire.setMaterial(mat_red);
        fire.setImagesX(2);
        fire.setImagesY(2);
        fire.setEndColor(new ColorRGBA(0f, 0f, 1f, 0.0f));
        fire.setStartColor(new ColorRGBA(0f, 1f, 0f, 0.5f));
        fire.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 25, 0));
        fire.setStartSize(3);
        fire.setEndSize(0.01f);
        fire.setGravity(0, 9.8f, 0);
        fire.setLowLife(1f);
        fire.setHighLife(5f);
        fire.getParticleInfluencer().setVelocityVariation(0.75f);
        fire.emitAllParticles();
        fire.addControl(new ParticleFader());
        rootNode.attachChild(fire);
    }

    // Make the goal in the center of the world
    private Spatial makeTeapot() {
        // OPTION 1
        //Spatial teapot = assetManager.loadModel("Models/Teapot/Teapot.obj");
        //Material mat_default = new Material( assetManager, "Common/MatDefs/Misc/ShowNormals.j3md");
        //teapot.setMaterial(mat_default);
        //teapot.scale(20);
        // OPTION 1

        // OPTION 2
        //Spatial teapot = assetManager.loadModel("Models/Boat/boat.j3o");
        // OPTION 2
        // OPTION 3
        //Spatial teapot = assetManager.loadModel("Models/Buggy/Buggy.j3o");
        // OPTION 3
        // OPTION 4
        //Spatial teapot = assetManager.loadModel("Models/Elephant/Elephant.mesh.xml");
        // OPTION 4
        // OPTION 5
        Spatial teapot = assetManager.loadModel("Models/Tree/Tree.mesh.j3o");
        teapot.scale(20);
        // OPTION 5

        teapot.setName(TEAPOT_TNAME);
        RigidBodyControl rigidBodyControl = new RigidBodyControl(new SphereCollisionShape(8), 0);
        teapot.addControl(rigidBodyControl);
        bulletAppState.getPhysicsSpace().add(teapot);
        rootNode.attachChild(teapot);
        return teapot;
    }

    // create an enemy at the given location
    private Spatial spawnEnemy(Vector3f location) {
        // Make sure the enemy isn't going to appear too close to the goal
        float distanceToTeaPot = location.distance(Vector3f.ZERO);
        if (distanceToTeaPot < MINIMUM_SPAWN_DISTANCE) {
            location.addLocal(MINIMUM_SPAWN_DISTANCE, 0, MINIMUM_SPAWN_DISTANCE);
        }
        Node container = new Node(ENEMY_CONTAINER_NAME);
        Node enemy = (Node) assetManager.loadModel("Models/Oto/Oto.mesh.xml");
        enemy.scale(2);
        enemy.setName("Enemy");
        container.attachChild(enemy);
        container.setLocalTranslation(location);
        enemy.setLocalTranslation(new Vector3f(0, 5, 0));
        BetterCharacterControl betterCharacterControl = new BetterCharacterControl(4, 10, 10);
        container.addControl(betterCharacterControl);
        bulletAppState.getPhysicsSpace().add(betterCharacterControl);
        container.addControl(new EnemyController());
        rootNode.attachChild(container);
        AnimControl control = enemy.getControl(AnimControl.class);
        AnimChannel channel = control.createChannel();
        channel.setAnim("Walk", 0.50f);
        channel.setLoopMode(LoopMode.Loop);
        enemyCount++;
        return container;
    }

    // create the whole world (floor, walls, goal)
    private Spatial makeWorld() {
        rootNode.attachChild(SkyFactory.createSky(
                assetManager, "Textures/Sky/Bright/BrightSky.dds", false));
        initLighting();
        Box box = new Box(500, 1, 500);
        Geometry floor = new Geometry("Box", box);
        floor.setLocalTranslation(new Vector3f(0, -10, 0));
        RigidBodyControl rigidBodyControl = new RigidBodyControl(0);
        floor.addControl(rigidBodyControl);
        bulletAppState.getPhysicsSpace().add(rigidBodyControl);
        Material floorMaterial = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");

        // OPTION 1
        //Texture tex = assetManager.loadTexture("Textures/Terrain/Rock/Rock.PNG");
        //Texture normal = assetManager.loadTexture("Textures/Terrain/Rock/Rock_normal.png");
        // OPTION 1
        // OPTION 2
        //Texture tex = assetManager.loadTexture("Textures/Terrain/Pond/Pond.jpg");
        //Texture normal = assetManager.loadTexture("Textures/Terrain/Pond/Pond_normal.png");
        // OPTION 2
        // OPTION 3
        //Texture tex = assetManager.loadTexture("Textures/Terrain/Rock2/rock.jpg");
        //Texture normal = assetManager.loadTexture("Textures/Terrain/Rock2/rock.jpg");
        // OPTION 3
        // OPTION 4
        Texture tex = assetManager.loadTexture("Textures/Terrain/Rocky/RockyTexture.jpg");
        Texture normal = assetManager.loadTexture("Textures/Terrain/Rocky/RockyNormals.jpg");
        // OPTION 4

        floorMaterial.setTexture("DiffuseMap", tex);
        floorMaterial.setTexture("NormalMap", normal);
        floorMaterial.setBoolean("UseMaterialColors", true);
        floorMaterial.setColor("Diffuse", FLOOR_COLOUR);
        floorMaterial.setColor("Specular", FLOOR_SPECULAR);
        floorMaterial.setFloat("Shininess", FLOOR_SHININESS);
        floor.setMaterial(floorMaterial);
        rootNode.attachChild(floor);
        Random r = new Random();
        for (int i = -5; i < 5; i++) {
            for (int j = -5; j < 5; j++) {
                makeWall(new Vector3f(i * BLOCK_SPACING, 10, j * BLOCK_SPACING), 5, 10, 5);
            }
        }
        makeTeapot();
        return floor;
    }

    // create an obstacle
    private Spatial makeWall(Vector3f location, int x, int y, int z) {

        // OPTION 1
        //Box model = new Box(x, y, z);
        // OPTION 1
        // OPTION 2
        Sphere model = new Sphere(x * 3, y * 3, z * 2);
        // OPTION 2

        Spatial wall = new Geometry("Box", model);
        wall.setName(WALL_NAME);
        wall.setLocalTranslation(location);
        RigidBodyControl rigidBodyControl = new RigidBodyControl(0);
        wall.addControl(rigidBodyControl);
        bulletAppState.getPhysicsSpace().add(rigidBodyControl);
        Material mat_brick = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        mat_brick.setTexture("DiffuseMap", assetManager.loadTexture("Textures/Terrain/BrickWall/BrickWall.jpg"));
        mat_brick.setTexture("NormalMap", assetManager.loadTexture("Textures/Terrain/BrickWall/BrickWall.jpg"));
        mat_brick.setBoolean("UseMaterialColors", true);
        mat_brick.setColor("Diffuse", BLOCK_COLOUR);
        mat_brick.setColor("Specular", BLOCK_SPECULAR);
        mat_brick.setFloat("Shininess", BLOCK_SHININESS);
        wall.setMaterial(mat_brick);
        rootNode.attachChild(wall);
        return wall;
    }

    // check for collisions between physics shapes
    public void collision(PhysicsCollisionEvent event) {
        Spatial cannonBall = resolve(event.getNodeA(), event.getNodeB(), CANNON_BALL_NAME);
        Spatial enemy = resolve(event.getNodeA(), event.getNodeB(), ENEMY_CONTAINER_NAME);
        Spatial teapot = resolve(event.getNodeA(), event.getNodeB(), TEAPOT_TNAME);

        if (cannonBall != null) {
            makeFire(cannonBall.getLocalTranslation());
            makeDebris(cannonBall.getLocalTranslation());
            explosion(cannonBall.getLocalTranslation());
            removeObject(cannonBall);
        }

        if (teapot != null) {
            if (enemy != null) {
                gameOver();
            }
        }

    }

    // Show a message when game over and remove all game objects
    private void gameOver() {
        killText.setText(LOSER_MSG);
        rootNode.detachAllChildren();
    }

    // runs approximately 60 times per second to update some game state
    @Override
    public void simpleUpdate(float tpf) {
        // increment weapon timers
        shootTimer += tpf;
        shootAltTimer += tpf;
        // set kill text
        killText.setText(String.valueOf(kills));
        // spawn in more enemies if we go below the minimum number
        if (enemyCount < NUM_ENEMIES) {
            spawnEnemy(getRandomVector(400, 15));
        }
    }

    // cast a ray into the scene based on camera position and direction to see if we hit something
    public void fireRay(Vector3f origin, Vector3f direction) {
        CollisionResults hits = new CollisionResults();
        Ray ray = new Ray(origin, direction);
        rootNode.collideWith(ray, hits);
        if (hits.getClosestCollision() != null) {
            // check if the closest thing we hit was an enemy model
            if (hits.getClosestCollision().getGeometry().getName().equals("Oto-geom-1")) {
                Spatial parent = hits.getClosestCollision().getGeometry().getParent().getParent();
                killEnemey(parent);
                return;
            }
        }
    }

    // create a cannonball
    public Spatial fireProjectile(Vector3f origin, Vector3f direction) {
        Sphere sphere = new Sphere(32, 32, 10);
        Geometry cannonBall = new Geometry(CANNON_BALL_NAME, sphere);
        cannonBall.setLocalTranslation(origin);
        RigidBodyControl rigidBodyControl = new RigidBodyControl(100);
        cannonBall.addControl(rigidBodyControl);
        bulletAppState.getPhysicsSpace().add(rigidBodyControl);
        rigidBodyControl.setLinearVelocity(direction.mult(CANNON_BALL_SPEED));
        Material cannonBallMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        cannonBallMat.setColor("Color", ColorRGBA.Black);
        cannonBall.setMaterial(cannonBallMat);
        rootNode.attachChild(cannonBall);
        return cannonBall;
    }

    // setup the input 
    private void initInput() {
        inputManager.addMapping("Shoot", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        inputManager.addMapping("Shoot_alt", new MouseButtonTrigger(MouseInput.BUTTON_RIGHT));
        inputManager.addMapping("Quit", new KeyTrigger(KeyInput.KEY_ESCAPE));
        inputManager.addListener(analogListener, "Shoot", "Shoot_alt", "Quit");
    }
    private AnalogListener analogListener = new AnalogListener() {
        public void onAnalog(String name, float value, float tpf) {
            if (name.equals("Shoot")) {
                if (shootTimer >= SHOOT_REFIRE_TIME) {
                    shootTimer = 0;
                    fireRay(cam.getLocation(), cam.getDirection());
                    audio_gun.playInstance();
                }
            } else if (name.equals("Shoot_alt")) {
                if (shootAltTimer >= SHOOT_ALT_REFIRE_TIME) {
                    shootAltTimer = 0;
                    fireProjectile(cam.getLocation(), cam.getDirection());
                    audio_gun.playInstance();
                }
            } else if (name.equals("Quit")) {
                stop();
            }

        }
    };

}
